<?php

namespace App\Repository;

class Query
{
	private $columns = [];
	private $where = [];
	private $order = [];
	private $limit;
	private $offset;

	public function __construct(array $columns, array $where, array $order, $limit, $offset)
	{
		$this->columns = $columns;
		$this->where = $where;
		$this->order = $order;
		$this->limit = $limit;
		$this->offset = $offset;
	}

	public function toArray()
	{
		$result = [];

		if ($this->columns) {
			$result['columns'] = $this->columns;
		}

		if ($this->where) {
			$result['where'] = Where::create($this->where)->toArray();
		}

		if ($this->order) {
			$result['order'] = $this->order;
		}

		if ($this->limit) {
			$result['limit'] = $this->limit;
		}

		if ($this->limit && $this->offset) {
			$result['offset'] = $this->offset;
		}

		return $result;
	}
}