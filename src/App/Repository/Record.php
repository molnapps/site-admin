<?php

namespace App\Repository;

class Record
{
	private $map;
	private $table;
	private $assignments;

	public function __construct(Map $map, TableGateway $tableGateway, Assignments $assignments)
	{
		$this->map = $map;
		$this->table = $tableGateway;
		$this->assignments = $assignments;
	}

	public function save()
	{
		if ($this->exists()) {
			$this->update();
		} else {
			$this->insert();
		}
	}

	public function insert()
	{
		$this->setAssignment($this->map->getCreatedAtKey(), $this->getFreshTimestamp());

		$this->table->insert($this->getInsertAssignments());

		$this->setId($this->table->lastInsertId());
	}

	public function update()
	{
		$this->setAssignment($this->map->getUpdatedAtKey(), $this->getFreshTimestamp());

		$this->table->update($this->getUpdateAssignments(), $this->getIdentity());
	}

	public function delete()
	{
		if ( ! $this->exists() || $this->isLocked()) {
			return;
		}

		$this->table->delete($this->getIdentity());
	}

	public function trash()
	{
		if ( ! $this->exists()) {
			return;
		}

		$this->setAssignment($this->map->getDeletedAtKey(), $this->getFreshTimestamp());

		$this->table->update($this->getTrashAssignments(), $this->getIdentity());
	}

	public function restore()
	{
		if ( ! $this->isTrashed()) {
			return;
		}

		$this->setAssignment($this->map->getDeletedAtKey(), null);

		$this->table->update($this->getTrashAssignments(), $this->getIdentity());
	}

	public function isNew()
	{
		return ! $this->exists();
	}

	public function exists()
	{
		return !! $this->getAssignment($this->map->getPrimaryKey());
	}

	public function isTrashed()
	{
		return !! $this->getAssignment($this->map->getDeletedAtKey());
	}

	public function isDeletable()
	{
		return $this->map->isDeletable();
	}

	public function isLocked()
	{
		return ! $this->isDeletable();
	}

	private function getInsertAssignments()
	{
		return array_merge(
			$this->assignments->getAssignments(), 
			[$this->map->getCreatedAtKey() => $this->getFreshTimestamp()]
		);
	}

	private function getUpdateAssignments()
	{
		$assignments = [];

		foreach ($this->assignments->getAssignments() as $assignment => $value) {
			if ($this->assignments->isDirty($assignment)) {
				$assignments[$assignment] = $value;
			}
		}

		if ($assignments) {
			$assignments[$this->map->getUpdatedAtKey()] = $this->getFreshTimestamp();
		}

		return $assignments;
	}

	private function getTrashAssignments()
	{
		$deletedAtKey = $this->map->getDeletedAtKey();
		return [$deletedAtKey => $this->assignments->getAssignment($deletedAtKey)];
	}

	private function getIdentity()
	{
		$primaryKey = $this->map->getPrimaryKey();
		
		return [$primaryKey => $this->getAssignment($primaryKey)];
	}

	private function getFreshTimestamp()
	{
		return gmdate('Y-m-d H:i:s');
	}

	private function setId($id)
	{
		return $this->setAssignment($this->map->getPrimaryKey(), $id);
	}

	protected function getAssignment($assignment)
	{
		return $this->assignments->getAssignment($assignment);
	}

	protected function setAssignment($assignment, $value)
	{
		return $this->assignments->setAssignment($assignment, $value);
	}
}