<?php

namespace App\Repository;

use \App\Repository\Scope\Scope;

class Repository
{
	private $map;
	private $table;
	private $collection;

	private $queryBuilder;

	private $scopes = [];

	public function __construct(
		Map $map, 
		TableGateway $tableGateway, 
		Collection $collection, 
		Assignments $assignments = null
	) {
		$this->map = $map;
		$this->table = $tableGateway;
		$this->collection = $collection;
		$this->assignments = $assignments;
	}

	public function __call($name, $args)
	{
		if (method_exists($this->getQueryBuilder(), $name)) {
			call_user_func_array([$this->getQueryBuilder(), $name], $args);
			return $this;
		}

		throw new \Exception('Unknown method ' . $name);
	}

	public function registerGlobalScope(Scope $scope)
	{
		$this->scopes[] = $scope;

		return $this;
	}

	public function findById($id)
	{
		if ( ! $id) {
			return $this->hydrate([])->getFirst();
		}

		$query = $this
			->getQueryBuilder()
			->where([$this->map->getPrimaryKey() => $id])
			->limit(1)
			->getQuery();

		$rows = $this->table->select($query->toArray());
		
		return $this->hydrate($rows)->getFirst();
	}

	public function find()
	{
		$query = $this
			->getQueryBuilder()
			->getQuery();

		$rows = $this->table->select($query->toArray());

		return $this->hydrate($rows);
	}

	public function save(Assignments $assignments = null)
	{
		return $this->getRecord($assignments)->save();
	}

	public function trash(Assignments $assignments = null)
	{
		return $this->getRecord($assignments)->trash();
	}

	public function restore(Assignments $assignments = null)
	{
		return $this->getRecord($assignments)->restore();
	}

	public function isNew(Assignments $assignments = null)
	{
		return $this->getRecord($assignments)->isNew();
	}

	public function exists(Assignments $assignments = null)
	{
		return $this->getRecord($assignments)->exists();
	}
	
	public function isTrashed(Assignments $assignments = null)
	{
		return $this->getRecord($assignments)->isTrashed();
	}
	
	public function isDeletable(Assignments $assignments = null)
	{
		return $this->getRecord($assignments)->isDeletable();
	}
	
	public function isLocked(Assignments $assignments = null)
	{
		return $this->getRecord($assignments)->isLocked();
	}

	private function getRecord(Assignments $assignments = null)
	{
		$assignments = ($assignments) ?: $this->assignments;
		return new Record($this->map, $this->table, $assignments);
	}

	private function hydrate(array $rows)
	{
		$this->resetQueryBuilder();

		return $this->createCollection($rows);
	}

	private function createCollection(array $rows)
	{
		return $this->collection->createNewCollection($rows);
	}

	private function getQueryBuilder()
	{
		if ( ! $this->queryBuilder) {
			$this->queryBuilder = QueryBuilder::create();
			$this->applyGlobalScopes();
		}

		return $this->queryBuilder;
	}

	private function applyGlobalScopes()
	{
		foreach ($this->scopes as $scope) {
			$scope->apply($this->queryBuilder);
		}
	}

	private function resetQueryBuilder()
	{
		$this->queryBuilder = null;
	}
}