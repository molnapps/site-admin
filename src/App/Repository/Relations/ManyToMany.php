<?php

namespace App\Repository\Relations;

use \App\Repository\TableGateway;
use \App\Repository\ActiveRecord;
use \App\Repository\Collection;

class ManyToMany implements Relation
{
	private $pivotTable;

	private $parent;
	private $relative;
	
	public function __construct(
		TableGateway $pivotTable, 
		Relative $parent, 
		Relative $relative,
		ActiveRecord $relativeRepository
	) {
		$this->pivotTable = $pivotTable;
		$this->parent = $parent;
		$this->relative = $relative;
		$this->relativeRepository = $relativeRepository;
	}
	
	public function attach(Relative $relative = null)
	{
		if ( ! $relative) {
			return;
		}
		
		return $this->pivotTable->insert([
			$this->parent->getForeignKey() => $this->parent->getId(),
			$this->relative->getForeignKey() => $relative->getId(),
		]);
	}

	public function detach(Relative $relative = null)
	{
		if ( ! $relative) {
			return;
		}

		return $this->pivotTable->delete([
			$this->parent->getForeignKey() => $this->parent->getId(),
			$relative->getForeignKey() => $relative->getId()
		]);
	}

	public function reset()
	{
		if ( ! $this->parent->getId()) {
			throw new \Exception('You cannot reset all parent ids!');
		}

		return $this->pivotTable->delete([
			$this->parent->getForeignKey() => $this->parent->getId()
		]);
	}

	public function get()
	{
		$rows = $this->pivotTable->select(['where' => $this->getParentScope()]);

		$relativesMap = $this->groupParentIds($rows);
		$relativesCollection = $this->getRelativesCollection($rows);

		$collectionsMap = $this->convertIdsToCollection($relativesMap, $relativesCollection);

		if ($this->parent->getId()) {
			return $collectionsMap[$this->parent->getId()];
		}

		return $collectionsMap;
	}

	private function getParentScope()
	{
		$where = [];
		
		if ($this->parent->getId()) {
			$where[] = [$this->parent->getForeignKey(), 'eq', $this->parent->getId()];
		}

		return $where;
	}

	private function getRelativesCollection(array $rows)
	{
		$relativeIds = $this->getUniqueRelativeIds($rows);

		if ( ! $relativeIds) {
			return $this->relativeRepository->createCollection();
		}

		$relativesCollection = $this->relativeRepository
			->getRepository()
			->where([
				[$this->relative->getPrimaryKey(), 'in', $relativeIds]
			])
			->find();

		return $relativesCollection;
	}

	private function getUniqueRelativeIds($rows)
	{
		$result = [];

		foreach ($rows as $row) {
			$result[] = $row[$this->relative->getForeignKey()];
		}

		return array_unique($result);
	}

	private function groupParentIds($rows)
	{
		$result = [];

		foreach ($rows as $row) {
			$result[$row[$this->parent->getForeignKey()]][] = $row[$this->relative->getForeignKey()];
		}

		if ( $this->parent->getId() && ! isset($result[$this->parent->getId()])) {
			$result[$this->parent->getId()] = [];
		}
		
		return $result;
	}

	private function convertIdsToCollection(array $relativesMap, Collection $relativesCollection)
	{
		$result = [];

		foreach ($relativesMap as $parentId => $relativeIds) {
			$result[$parentId] = $relativesCollection->filterCollection('id', $relativeIds);
		}

		return $result;
	}
}