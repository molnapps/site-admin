<?php

namespace App\Repository\Relations;

interface Relative
{
	public function getPrimaryKey();
	public function getForeignKey();
	public function getId();
	public function getRelativeName();
}