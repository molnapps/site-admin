<?php

namespace App\Repository;

interface TableGateway
{
	public function select(array $query);
	public function insert(array $assignments);
	public function lastInsertId();
	public function update(array $assignments, array $query);
	public function delete(array $query);
}