<?php

namespace App\Repository;

interface ActiveRecord
{
	public function save();
	public function trash();
	public function restore();
	
	public function findById($id);
	public function find();

	public function getRepository();
}