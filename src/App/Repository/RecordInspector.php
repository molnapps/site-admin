<?php

namespace App\Repository;

interface RecordInspector
{
	public function isNew();
	public function exists();
	public function isTrashed();
	public function isDeletable();
	public function isLocked();
}