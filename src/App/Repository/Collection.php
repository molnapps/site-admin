<?php

namespace App\Repository;

interface Collection
{
	public function createNewCollection(array $rows);
	public function filterCollection($key, array $values);
}