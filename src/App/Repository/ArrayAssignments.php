<?php

namespace App\Repository;

class ArrayAssignments implements Assignments
{
	private $properties;
	
	public function __construct(array $properties = [])
	{
		$this->properties = ($properties) ?: ['id' => 1, 'body' => 'lorem ipsum dolor'];
	}

	public function getAssignments()
	{
		return $this->properties;;
	}

	public function getAssignment($property)
	{
		return (isset($this->properties[$property])) ? $this->properties[$property] : null;
	}

	public function setAssignment($property, $value)
	{
		$this->properties[$property] = $value;
	}

	public function isDirty($property)
	{
		return ! ($property == 'id');
	}
}