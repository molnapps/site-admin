<?php

namespace App\Repository;

class Map
{
	private $isDeletable = true;

	public function getPrimaryKey()
	{
		return 'id';
	}

	public function getCreatedAtKey()
	{
		return 'created_at';
	}

	public function getUpdatedAtKey()
	{
		return 'updated_at';
	}

	public function getDeletedAtKey()
	{
		return 'deleted_at';
	}

	public function isDeletable()
	{
		return $this->isDeletable;
	}

	public function setIsDeletable($bool)
	{
		$this->isDeletable = !! $bool;
	}
}