<?php

namespace App\Validation;

class WhitelistFilter implements Filter
{
	private $whitelist;
	
	public function __construct(array $whitelist)
	{
		$this->whitelist = $whitelist;
	}

	public function filter($value)
	{
		return in_array($value, $this->whitelist) ? $value : null;
	}
}