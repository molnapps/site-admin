<?php

namespace App\Control;

use \MolnApps\Control\CommandsMap\CommandStatuses;

class AdminCommandStatuses extends CommandStatuses
{
	public function __construct()
	{
		parent::__construct([
			'CMD_VALIDATION_ERROR' => -5,
			'CMD_INVALID_REQUEST' => -4,
			'CMD_NOT_FOUND' => -3,
			'CMD_AUTH_ERROR' => -2,
			'CMD_ERROR' => -1,
			'CMD_DEFAULT' => 0,
			'CMD_OK' => 1,
			'CMD_CHOOSE_LANG' => 2,
		]);
	}
}