<?php

namespace App\Control;

class AdminView
{
	private $viewNames;

	public function __construct(array $viewNames)
	{
		$this->viewNames = $viewNames;
	}
	
	public function invoke(array $args = [])
	{
		ob_start(); 

		foreach ($this->viewNames as $view) {
			if ( ! $this->viewExists($view)) {
				throw new \Exception("View file {$view} does not exist.");
			}
			$this->includeView($view, $args);
		}

		return ob_get_clean();
	}

	private function viewExists($view)
	{
		return file_exists($this->getPath($view));
	}

	private function includeView($view, array $args = [])
	{
		extract($args);
		return include($this->getPath($view));
	}

	private function getPath($view)
	{
		$view = str_replace('.', '/', $view);
		return $this->getBasePath() . $view . '.php';
	}

	private function getBasePath()
	{
		return __DIR__ . '/../../../public/_views/';
	}
}