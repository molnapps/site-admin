<?php

namespace App\Control;

use \MolnApps\Testing\Response\Controller;
use \App\Control\Session\SessionProvider;
use \App\Control\AdminRequest;
use \App\Base\AppContainer;

class AdminTestController implements Controller
{
	public function bootApplication()
	{
		
	}

	public function shutdownApplication()
	{
		SessionProvider::getSession()->destroy();
		SessionProvider::reset();
		AdminRequest::reset();
		AppContainer::get('languageProvider')->reset();
		$this->shutdownAfterRequest();
	}

	private function shutdownAfterRequest()
	{
		AppContainer::reset();
		AdminRequest::reset();
	}

	public function run($requestParams = [], $uploadedFiles = [])
	{
		$request = AdminRequest::instance();
		
		foreach ($requestParams as $param => $value) {
			$request->$param = $value;
		}

		$uploadedFiles = array_filter($uploadedFiles);
		$request->files()->add($uploadedFiles);

		$response = \App\Control\AdminController::run($request);

		$this->shutdownAfterRequest();
		
		return $response;
	}

	public function signedInAs($user)
	{
		SessionProvider::getSession()->signedIn = 1;
	}

	public function signedOut()
	{
		SessionProvider::getSession()->signedIn = 0;
	}
}