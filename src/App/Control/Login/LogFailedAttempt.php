<?php

namespace App\Control\Login;

class LogFailedAttempt
{
	private $errorLog;
	private $failedAttemptsManager;

	public function __construct(ErrorLog $errorLog, FailedAttemptsManager $failedAttemptsManager)
	{
		$this->errorLog = $errorLog;
		$this->failedAttemptsManager = $failedAttemptsManager;
	}

	public function handle()
	{
		if ($this->failedAttemptsManager->greaterThan(5)) {
			$this->errorLog->addError(static::class, 'Too many attempts');
			return;
		}
		if ($this->errorLog->hasErrors(VerifyCredentials::class)) {
			$this->failedAttemptsManager->increment();
		} else {
			$this->failedAttemptsManager->reset();
		}
	}
}