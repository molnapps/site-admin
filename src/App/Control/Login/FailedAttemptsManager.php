<?php

namespace App\Control\Login;

use \App\Base\AppContainer;

class FailedAttemptsManager
{
	private $delay = 60 * 5;

	private $driver;

	public function __construct(FailedAttemptsDriver $driver)
	{
		$this->driver = $driver;
	}

	public function reset()
	{
		$this->driver->reset();
	}

	public function increment()
	{
		$this->driver->increment();
	}

	public function greaterThan($count)
	{
		return $this->driver->greaterThan($count, $this->delay);
	}
}