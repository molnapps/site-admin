<?php

namespace App\Control\Login;

interface FailedAttemptsDriver
{
	public function reset();
	public function increment();
	public function greaterThan($count, $delay);
}