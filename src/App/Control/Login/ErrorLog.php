<?php

namespace App\Control\Login;

class ErrorLog
{
	private $errors = [];

	public function addError($delegateClass, $message)
	{
		$this->errors[] = [$delegateClass, $message];
	}

	public function hasErrors($delegateClass = null)
	{
		if ($delegateClass) {
			return $this->hasErrorsForDelegate($delegateClass);
		}
		return !! $this->errors;
	}

	private function hasErrorsForDelegate($delegateClass)
	{
		return !! array_filter($this->errors, function($el) use ($delegateClass) {
			return $el[0] == $delegateClass;
		});
	}
}