<?php

namespace App\Control\Request;

use \App\Form\FormValidator;

trait ValidationLog
{
	private $messages = [];

	public function validate(array $rules, $domainObject)
	{
		return (new FormValidator($this, $domainObject))->validate($rules);
	}

	public function addValidationError($error)
	{
		$error = (object)$error;
		
		$this->messages[] = (object)[
			'identifier' => $error->identifier, 
			'body' => $error->message, 
			'type' => 'error'
		];
	}

	public function hasValidationErrors()
	{
		return !! $this->messages;
	}

	public function getValidationMessages($identifier)
	{
		$result = [];

		foreach ($this->messages as $message) {
			if ($message->identifier == $identifier) {
				$result[] = $message;
			}
		}
		
		return $result;
	}
}