<?php

namespace App\Control;

use \MolnApps\Control\AppController\AppController;
use \MolnApps\Control\AppController\CommandResolver;
use \MolnApps\Control\AppController\MiddlewareController;

use \App\Base\AppContainer;

use \App\Commands\CommandsFactory;

use \MolnApps\Control\Middleware\CommandMiddleware as CommandsMiddleware;

use \App\Commands\Middleware\AuthMiddleware;
use \App\Commands\Middleware\CsrfMiddleware;
use \App\Commands\Middleware\MethodMiddleware;
use \App\Commands\Middleware\LanguageMiddleware;

class AdminController
{
	public static function run(AdminRequest $request = null)
	{
		return (new static)->handle($request);
	}

	public function handle(AdminRequest $request = null)
	{
		if ( ! AppContainer::get('env')->env('test')) {
			AppContainer::get('languageProvider')->setAvailableLanguages(
				config()->language->available->toArray()
			);
		}

		AppContainer::get('session')->start();
		
		$request = $request ?: AdminRequest::instance();

		$appController = $this->createAppController($request);
		$appController->executeCommandsChain($request->cmd, 'DefaultCommand');

		return $this->invokeView($appController);
	}

	private function createAppController(AdminRequest $request)
	{
		$commandsMap = $this->createCommandsMap();

		$commandsFactory = new CommandsFactory($request);
		$commandsMiddleware = new CommandsMiddleware($commandsMap);

		$commandsMiddleware->register('auth', new AuthMiddleware);
		$commandsMiddleware->register('csrf', new CsrfMiddleware($request));
		$commandsMiddleware->register('post', new MethodMiddleware($request, 'post'));
		$commandsMiddleware->register('language', new LanguageMiddleware());

		return new AppController(
			new CommandResolver($commandsMap, $commandsFactory), 
			new MiddlewareController($commandsMap, $commandsMiddleware)
		);
	}

	private function invokeView(AppController $appController)
	{
		$viewNamesArray = $appController->getQualifiedViews();
		$viewNamesArray = array_merge(['header'], $viewNamesArray, ['footer']);
		return (new AdminView($viewNamesArray))->invoke();
	}

	private function createCommandsMap()
	{
		return (new AdminCommandsMapProvider)->getCommandsMap();
	}
}