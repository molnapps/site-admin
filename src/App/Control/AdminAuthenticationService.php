<?php

namespace App\Control;

use \App\Control\Session\Session;
use \App\Control\Session\SessionProvider;

use \App\Base\AppContainer;

use \App\Control\Login\ErrorLog;
use \App\Control\Login\VerifyCredentials;
use \App\Control\Login\LogFailedAttempt;

class AdminAuthenticationService
{
	private $session;

	public function __construct(Session $session = null)
	{
		$this->session = ($session) ?: SessionProvider::getSession();
	}

	public function authenticate($username, $password)
	{
		$errorLog = AppContainer::get('signin.errorLog');
		$delegates = [
			AppContainer::get('signin.verifyCredentials')->setCredentials($username, $password),
			AppContainer::get('signin.logFailedAttempt'),
		];
		foreach ($delegates as $delegate) {
			$delegate->handle();
		}
		if ( ! $errorLog->hasErrors()) {
			$this->preventSessionFixation();
			$this->session->signedIn = 1;
		}
	}

	private function preventSessionFixation()
	{
		$this->session->regenerateId();
	}

	public function signOut()
	{
		$this->session->signedIn = 0;
	}

	public function isSignedIn()
	{
		return $this->session->signedIn == 1;
	}
}