<?php

namespace App\Control\Session;

class Session
{
	private $adapter;

	public function __construct(SessionAdapter $adapter)
	{
		$this->adapter = $adapter;
	}

	public function start()
	{
		$this->adapter->start();
		$this->generateCsrfTokenIfNotGenerated();
	}

	public function generateCsrfTokenIfNotGenerated()
	{
		if ( ! $this->csrfToken) {
			$this->csrfToken = (new CsrfToken)->get();	
		}
	}

	public function __set($name, $value) {
		$this->adapter->set($name, $value);
	}

	public function __get($name)
	{
		return $this->adapter->get($name);
	}

	public function __call($name, $args)
	{
		return call_user_func_array([$this->adapter, $name], $args);
	}
}