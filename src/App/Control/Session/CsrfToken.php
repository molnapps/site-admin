<?php

namespace App\Control\Session;

use \App\Base\Environment;

class CsrfToken
{
	private $testToken = 'abc123';

	public function get()
	{
		return $this->isTesting() ? $this->getTestToken() : $this->getProductionToken();
	}

	private function isTesting()
	{
		return Environment::instance()->env('test');
	}

	private function getTestToken()
	{
		return $this->testToken;
	}

	private function getProductionToken()
	{
		return base64_encode(openssl_random_pseudo_bytes(16));
	}
}