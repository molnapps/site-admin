<?php

namespace App\Control\Session;

class ArraySessionAdapter implements SessionAdapter
{
	private static $attr = [];
	
	public function start()
	{
		
	}

	public function regenerateId()
	{
		
	}

	public function destroy()
	{
		static::$attr = [];
	}
	
	public function set($property, $value)
	{
		static::$attr[$property] = $value;
	}
	
	public function get($property)
	{
		if (isset(static::$attr[$property])) {
			return static::$attr[$property];
		}
	}
}