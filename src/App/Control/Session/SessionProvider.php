<?php

namespace App\Control\Session;

use \App\Base\Environment;

class SessionProvider
{
	private static $session;
	private static $adapter;

	public static function getSession()
	{
		if ( ! static::$session) {
			static::$session = new Session(static::getAdapter());
		}

		return static::$session;
	}

	public static function getAdapter()
	{
		if ( ! static::$adapter) {
			static::$adapter = static::createSessionAdapter();
		}

		return static::$adapter;
	}

	public static function reset()
	{
		static::$session = null;
		static::$adapter = null;
	}

	private static function createSessionAdapter()
	{
		return Environment::instance()->env('test') ? new ArraySessionAdapter : new PhpSessionAdapter;
	}
}