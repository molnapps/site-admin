<?php

namespace App\Api;

use \App\Domain\News;

class ResponseFactory
{
    private function __construct()
    {

    }

    public static function make($key) {
        return (new static)->makeResponseFactory($key);
    }

    private function makeResponseFactory($key)
    {
        if ($this->hasFactory($key)) {
            return $this->getFactoryInstance($key);
        }
        
        throw new \Exception("There is no response factory for key [{$key}]");
    }

    private function hasFactory($key)
    {
        return array_key_exists($key, $this->getMap());
    }

    private function getFactoryInstance($key)
    {
        $map = $this->getMap();
        
        return new $map[$key];
    }

    private function getMap()
    {
        return [
            News::class => NewsResponseFactory::class
        ];
    }
}