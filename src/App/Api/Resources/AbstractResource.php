<?php

namespace App\Api\Resources;

use App\Api\Contracts\Arrayable;
use App\Api\Contracts\Jsonable;

abstract class AbstractResource implements Jsonable, Arrayable
{
    public function toJson() : string {
        return json_encode($this->toArray());
    }
}