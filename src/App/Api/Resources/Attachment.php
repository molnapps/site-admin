<?php

namespace App\Api\Resources;

use App\Domain\Attachment as DomainAttachment;
use App\View\NewsViewHelper;

class Attachment extends AbstractResource
{
    private $attachment;

    public function __construct(DomainAttachment $attachment) {
        $this->attachment = $attachment;
    }

    public function toArray() : array {
        return [
            'html' => $html = $this->getHtml(),
            'type' => $this->getType($html)
        ];
    }

    private function getHtml()
    {
        $attachments = NewsViewhelper::attachmentsArray([$this->attachment], 'Scarica l\'allegato');
        $attachments = NewsViewHelper::wrapIframe($attachments, 'div', 'News__video');

        return implode('', $attachments);
    }

    private function getType($html)
    {
        return stripos($html, 'News__video') 
            ? 'youtube' 
            : '';
    }
}