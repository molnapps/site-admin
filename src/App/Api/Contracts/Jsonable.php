<?php

namespace App\Api\Contracts;

interface Jsonable {
    public function toJson() : string;
}