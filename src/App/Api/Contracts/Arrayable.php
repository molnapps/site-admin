<?php

namespace App\Api\Contracts;

interface Arrayable {
    public function toArray() : array;
}