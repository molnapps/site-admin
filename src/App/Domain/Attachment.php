<?php

namespace App\Domain;

class Attachment extends DomainObject
{
	protected $fillables = ['title', 'permalink', 'filename', 'size', 'mimetype'];
}