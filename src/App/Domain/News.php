<?php

namespace App\Domain;

use \App\Domain\Contracts\HasAttachments;

use \App\Domain\Traits\Publishable;
use \App\Domain\Traits\Languageable;

class News extends DomainObject implements HasAttachments
{
	use Publishable;
	use Languageable;

	protected $traits = [Publishable::class, Languageable::class];

	protected $fillables = ['subject', 'body', 'type', 'display_date'];

	protected function bootstrapFilters()
	{
		$this->setFilter('subject', \App\Validation\PlainTextFilter::class);
		$this->setFilter('body', \App\Validation\PlainTextFilter::class);
		$this->setFilter('type', new \App\Validation\WhitelistFilter(['event', 'news']));
		$this->setFilter('display_date', \App\Validation\TimestampFilter::class);
	}

	public function hasAttachment()
	{
		return count($this->attachments) > 0;
	}

	public function getExcerpt($maxLength = 100)
	{
		if(strlen($this->body) <= $maxLength) {
			return $this->body;
		}

		$end = "...";
		
		$excerpt   = substr($this->body, 0, $maxLength - strlen($end));
		$lastSpace = strrpos($excerpt, ' ');
		
		return substr($excerpt, 0, $lastSpace) . $end;
	}

	public function hasExcerpt($maxLength = 100)
	{
		return ! ($this->getExcerpt($maxLength) == $this->body);
	}

	public function attachments()
	{
		return $this->belongsToMany(new Attachment);
	}

	public function authors()
	{
		return $this->belongsToMany(new Author);
	}

	public static function publishedWithType($type, $count = 5)
	{
		return static::available()
			->where([
				['published_at', 'not', null],
				['type', 'eq', $type],
			])
			->orderBy(['display_date' => 'desc', 'published_at' => 'desc'])
			->limit($count)
			->find();
	}

	public function setDisplayDateFromForm($dd, $mm, $yy)
	{
		if ( ! $yy || ! $mm || ! $dd) {
			return;
		}

		$this->display_date = $yy . '-' . sprintf("%02d", $mm) . '-' .  sprintf("%02d", $dd) . ' ' .	'00:00:00';
	}

	public function getDisplayDateDay()
	{
		return $this->getDisplayDateCarbon('j');
	}

	public function getDisplayDateMonth()
	{
		return $this->getDisplayDateCarbon('n');
	}

	public function getDisplayDateYear()
	{
		return $this->getDisplayDateCarbon('Y');
	}

	private function getDisplayDateCarbon($format)
	{
		return $this->display_date ? \Carbon\Carbon::parse($this->display_date)->format($format) : null;
	}

	public static function upcomingEventsCount()
	{
		$now = gmdate('Y-m-d H:i:s');
		return static::available()->where([
			['display_date', 'gte', $now], 
			['type', 'eq', 'event']
		])->find()->count();
	}

	public function getAuthor() : Author
	{
		if ($this->isNew() || $this->authors->isEmpty()) {
			return new Author;
		}

		return $this->authors->getFirst();
	}
}