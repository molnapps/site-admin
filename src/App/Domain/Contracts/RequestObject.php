<?php

namespace App\Domain\Contracts;

interface RequestObject
{
	public function getObjectName();
}