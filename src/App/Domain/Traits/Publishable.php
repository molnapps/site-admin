<?php

namespace App\Domain\Traits;

trait Publishable
{
	protected function bootstrapPublishableTrait()
	{
		$this->addFillable('published_at');
		$this->setFilter('published_at', \App\Validation\TimestampFilter::class);
	}

	public function isPublished($bool = null)
	{
		if (is_null($bool)) {
			return $this->published_at != null;
		}
		
		if ($bool) {
			$this->published_at = gmdate('Y-m-d H:i:s');
		} else {
			$this->published_at = null;
		}
	}
}