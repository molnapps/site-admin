<?php

namespace App\Domain\Traits;

use \App\Validation\Filter;

trait Filterable
{
	protected $filters = [];

	protected function bootstrapFilters()
	{
		// Override this method
	}

	private function filter($identifier, $value)
	{
		if ( ! $this->hasFilters($identifier)) {
			return $value;
		}

		foreach ($this->getFilters($identifier) as $filter) {
			$value = $this->getOrCreateFilter($filter)->filter($value);
		}
		
		return $value;
	}
	
	private function hasFilters($identifier)
	{
		return isset($this->filters[$identifier]);
	}
	
	private function getFilters($identifier)
	{
		return (array)$this->filters[$identifier];
	}

	private function getOrCreateFilter($filter)
	{
		if (is_string($filter) && class_exists($filter)) {
			$filter = new $filter;
		}

		if ( ! $filter instanceof Filter) {
			throw new \InvalidArgumentException(sprintf('Filter should implement %s interface.', Filter::class));
		}

		return $filter;
	}

	public function setFilter($identifier, $filter)
	{
		if ( ! $this->hasFilters($identifier)) {
			$this->filters[$identifier] = [];
		}
		
		$this->filters[$identifier][] = $filter;
	}
}