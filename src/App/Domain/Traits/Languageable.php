<?php

namespace App\Domain\Traits;

trait Languageable
{
	protected function bootstrapLanguageableTrait()
	{
		$this->addFillable('language');
		$this->setFilter('language', \App\Validation\LanguageFilter::class);
	}

	protected function bootstrapRepositoryLanguageableTrait()
	{
		$this->getRepository()->registerGlobalScope(new \App\Domain\Scope\LanguageScope);
	}
}