<?php

namespace App\Domain\Traits;

trait Attributable
{
	private $originalProperties = [];
	private $properties = [];

	protected $fillables = [];

	public function initializeProperties(array $properties)
	{
		$this->setOriginalProperties($properties);
		$this->resetProperties();
		$this->setProperties($properties);
	}

	private function setOriginalProperties(array $properties)
	{
		$this->originalProperties = $properties;
	}

	private function resetProperties()
	{
		$this->properties = [];
	}

	public function setProperties(array $properties)
	{
		foreach ($properties as $property => $value) {
			$this->setProperty($property, $value);
		}
	}

	public function getProperties()
	{
		return $this->properties;
	}

	public function getFillableProperties()
	{
		if ( ! $this->getFillables()) {
			throw new \Exception('Please define some fillable properties');
		}

		$fillables = [];
		
		foreach ($this->getFillables() as $fillable) {
			$fillables[$fillable] = $this->getProperty($fillable);
		}
		
		return $fillables;
	}

	protected function addFillable($property)
	{
		$this->fillables[] = $property;
	}

	private function getFillables()
	{
		return $this->fillables;
	}

	protected function bootstrapFillables()
	{
		// Override this method
	}

	public function isDirty($property)
	{
		return ($this->getOriginalProperty($property) != $this->getProperty($property));
	}

	public function getProperty($property)
	{
		if ($this->propertyExists($property)) {
			return $this->properties[$property];
		}
	}

	public function setProperty($property, $value)
	{
		$this->properties[$property] = $this->filterProperty($property, $value);
	}

	protected function filterProperty($property, $value)
	{
		return $value;
	}

	public function propertyExists($property)
	{
		return isset($this->properties[$property]);
	}

	public function hasProperty($property)
	{
		return !! $this->getProperty($property);
	}

	private function getOriginalProperty($property)
	{
		if (isset($this->originalProperties[$property])) {
			return $this->originalProperties[$property];
		}
	}
}