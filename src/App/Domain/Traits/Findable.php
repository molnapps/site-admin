<?php

namespace App\Domain\Traits;

trait Findable
{
	public static function findOrCreate($id)
	{
		$domainObject = static::newInstance()->findById($id);
		
		if ( ! $domainObject) {
			$domainObject = static::newInstance();
		}

		return $domainObject;
	}

	public static function findOrFail($id)
	{
		$domainObject = static::newInstance()->findById($id);
		
		if ( ! $domainObject) {
			throw new \Exception('Could not find ' . static::getObjectClass() . ' with id ' . $id);
		}

		return $domainObject;
	}

	private static function getObjectClass()
	{
		return get_class(static::newInstance());
	}

	public static function available()
	{
		return static::newInstance()
			->query()
			->where([['deleted_at', '=', null]]);
	}

	public static function trashed()
	{
		return static::newInstance()
			->query()
			->where([['deleted_at', '!=', null]]);
	}

	private static function newInstance()
	{
		return new static;
	}
}