<?php

namespace App\Domain\Traits;

use \App\Repository\Record;
use \App\Repository\Repository;
use \App\Repository\Map;

use \App\Repository\DomainObjectAssignments;

use \App\Database\TableGateway;
use \App\Database\TableGatewayFactory;

trait Recordable
{
	private $assignments;
	private $repository;

	private $tableGateway;

	public function save()
	{
		return $this->getRepository()->save();
	}

	public function trash()
	{
		return $this->getRepository()->trash();
	}

	public function restore()
	{
		return $this->getRepository()->restore();
	}

	public function findById($id)
	{
		return $this->getRepository()->findById($id);
	}

	public function find()
	{
		return $this->getRepository()->find();
	}

	public function isNew()
	{
		return $this->getRepository()->isNew();
	}
	
	public function exists()
	{
		return $this->getRepository()->exists();
	}
	
	public function isTrashed()
	{
		return $this->getRepository()->isTrashed();
	}
	
	public function isDeletable()
	{
		return $this->getRepository()->isDeletable();
	}

	public function isLocked()
	{
		return $this->getRepository()->isLocked();
	}

	public function getRepository()
	{
		if ( ! $this->repository) {
			$this->repository = new Repository(
				$this->getMap(), 
				$this->getTableGateway(), 
				$this->createCollection(),
				$this->getAssignments()
			);
			
			$this->bootstrapRepository();
		}

		return $this->repository;
	}

	protected function bootstrapRepository()
	{
		// Override this
	}

	private function getAssignments()
	{
		if ( ! $this->assignments) {
			$this->assignments = new DomainObjectAssignments($this);
		}

		return $this->assignments;
	}

	private function getMap()
	{
		return new Map;
	}

	private function getTableGateway()
	{
		return ($this->tableGateway) ?: $this->createTableGateway();
	}

	private function createTableGateway()
	{
		return TableGatewayFactory::get($this->getTableName());
	}

	abstract protected function getTableName();

	abstract public function createCollection();

	public function setTableGateway(TableGateway $tableGateway = null)
	{
		$this->tableGateway = $tableGateway;

		return $this;
	}
}