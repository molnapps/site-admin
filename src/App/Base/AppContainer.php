<?php

namespace App\Base;

use Pimple\Container;

class AppContainer
{
	private static $instance;

	private $container;

	public function __construct()
	{
		$this->container = new Container();

		$this->container['env'] = function($c) {
			return \App\Base\Environment::instance();
		};

		$this->container['request'] = function($c) {
			return \App\Control\AdminRequest::instance();
		};

		$this->container['session'] = function ($c) {
			return \App\Control\Session\SessionProvider::getSession();
		};

		$this->container['languageProvider'] = function($c) {
			return new \App\Language\LanguageProvider($c['session']);
		};

		$this->container['signin.failedAttemptsManager'] = function ($c) {
		    return new \App\Control\Login\FailedAttemptsManager($c['signin.failedAttemptsDriver']);
		};

		$this->container['signin.failedAttemptsDriver'] = function ($c) {
			return new \App\Control\Login\SessionFailedAttemptsDriver($c['session']);
		};

		$this->container['signin.errorLog'] = function ($c) {
		    return new \App\Control\Login\ErrorLog;
		};

		$this->container['signin.verifyCredentials'] = function ($c) {
		    return new \App\Control\Login\VerifyCredentials($c['signin.errorLog']);
		};

		$this->container['signin.logFailedAttempt'] = function ($c) {
			return new \App\Control\Login\LogFailedAttempt($c['signin.errorLog'], $c['signin.failedAttemptsManager']);
		};

		$this->container['contentFormatterFactory'] = function($c) {
			return new \App\View\ContentFormatterFactory;
		};
	}

	public static function get($identifier)
	{
		return static::instance()->container[$identifier];
	}

	public static function instance()
	{
		if ( ! static::$instance) {
			static::$instance = new static;
		}
		return static::$instance;
	}

	public static function reset()
	{
		static::$instance = null;
	}
}