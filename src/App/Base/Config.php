<?php

namespace App\Base;

use \Zend\Config\Config as ZendConfig;

class Config extends ZendConfig
{
	private static $instance;
	private static $config;

	private static $defaultConfig;
	private static $envConfig = [];

	public static function registerDefaultConfig($pathToFile)
	{
		static::$defaultConfig = $pathToFile;
	}

	public static function registerEnvConfig($env, $pathToFile)
	{
		static::$envConfig[$env] = $pathToFile;
	}

	public static function setEnvironment($env)
	{
		$config = include(static::$defaultConfig);

		if (in_array($env, array_keys(static::$envConfig))) {
			$config = array_replace_recursive($config, include(static::$envConfig[$env]));
		}

		static::loadConfig($config);
	}

	public static function loadConfig(array $config)
	{
		static::$config = $config;
	}

	public static function instance()
	{
		if ( ! static::$instance) {
			static::$instance = new static(static::$config);
		}

		return static::$instance;
	}

	public static function setInstance(Config $instance = null)
	{
		static::$instance = $instance;
	}

	public static function reset()
	{
		static::$config = null;
		static::$defaultConfig = null;
		static::$envConfig = [];
		static::setInstance(null);
	}
}