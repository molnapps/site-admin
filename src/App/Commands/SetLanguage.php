<?php

namespace App\Commands;

use \App\Base\AppContainer;

class SetLanguage extends AbstractCommand
{
	protected function doExecute()
	{
		if ( ! $this->request->language) {
			return $this->statuses('CMD_DEFAULT');
		}

		$languageProvider = AppContainer::get('languageProvider');

		$languageProvider->setCurrentLanguage($this->request->language);

		if ( ! $languageProvider->getCurrentLanguage()) {
			return $this->statuses('CMD_ERROR');
		}

		return $this->statuses('CMD_OK');
	}
}