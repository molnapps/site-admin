<?php

namespace App\Commands;

use \App\Domain\News;
use \App\Domain\Author;

class EditNews extends AbstractCommand
{
	protected function doExecute()
	{
		$news = News::findOrCreate($this->request->news_id);

		if ($news->isNew() && $this->request->news_id) {
			return $this->statuses('CMD_NOT_FOUND');
		}

		$this->request->addObject($news);

		$authorsCollection = (new Author)->find();
		$this->request->addObject($authorsCollection);

		return $this->statuses('CMD_OK');
	}
}