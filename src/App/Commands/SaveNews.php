<?php

namespace App\Commands;

use \App\Domain\News;
use \App\Domain\Author;

class SaveNews extends AbstractCommand
{
	protected function doExecute()
	{
		if ( ! $this->validateNews()) {
			$this->request->addError('Missing required fields.');
			return $this->statuses('CMD_VALIDATION_ERROR');
		}

		$news = News::findOrCreate($this->request->news_id);

		$news->language = $this->getCurrentLanguage();
		$news->type = $this->request->type;
		$news->subject = $this->request->subject;
		$news->body = $this->request->body;
		$news->setDisplayDateFromForm(
			$this->request->publishDate_dd,
			$this->request->publishDate_mm,
			$this->request->publishDate_yy
		);

		$news->isPublished( !! $this->request->published);

		$this->request->addObject($news);
		
		$news->save();

		$news->authors()->reset();

		if ($this->request->author) {
			$author = Author::findBySlugOrFail($this->request->author);

			$news->authors()->attach($author);
		}

		return $this->statuses('CMD_OK');
	}

	private function validateNews()
	{
		return $this->request->validate([
			'subject' => 'required', 
			'body' => 'required', 
			'type' => 'required',
		], new News);
	}
}