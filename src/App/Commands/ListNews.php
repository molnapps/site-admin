<?php

namespace App\Commands;

use \App\Domain\News;

class ListNews extends AbstractCommand
{
	protected function doExecute()
	{
		$newsCollection = News::available()->orderBy(['id' => 'desc'])->limit(50)->find();

		$this->request->addObject($newsCollection);

		return $this->statuses('CMD_OK');
	}
}