<?php

namespace App\Commands\Middleware;

use \MolnApps\Control\Middleware\Middleware;

use \App\Base\AppContainer;

use \App\Control\AdminCommandStatuses;

class LanguageMiddleware implements Middleware
{
	public function authorize()
	{
		$languageProvider = AppContainer::get('languageProvider');

		if ($languageProvider->getCurrentLanguage()) {
			return;
		}

		if ($languageProvider->supportsOneLanguage()) {
			$languageProvider->setCurrentLanguage($languageProvider->getFirstAvailableLanguage());
			return;
		}

		return (new AdminCommandStatuses)->getStatus('CMD_CHOOSE_LANG');
	}
}