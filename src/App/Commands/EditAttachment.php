<?php

namespace App\Commands;

use \App\Domain\Attachment;
use \App\Domain\News;

class EditAttachment extends AbstractCommand
{
	protected function doExecute()
	{
		$attachment = Attachment::findOrCreate($this->request->id);

		if ($attachment->isNew() && $this->request->id) {
			return $this->statuses('CMD_NOT_FOUND');
		}
		
		$this->request->addObject($attachment);

		if ($this->request->news_id) {
			$news = News::findOrCreate($this->request->news_id);

			$this->request->addObject($news);
		}

		return $this->statuses('CMD_OK');
	}
}