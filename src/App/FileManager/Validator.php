<?php

namespace App\FileManager;

use \MolnApps\FileManager\Validator\AbstractValidator;

class Validator extends AbstractValidator
{
	protected function getAllowedMimeTypes()
	{
		return config()->uploads->allowedMimeTypes->toArray();
	}

	protected function getAllowedMaxSizeInKilobytes()
	{
		return config()->uploads->allowedMaxSize;
	}
}