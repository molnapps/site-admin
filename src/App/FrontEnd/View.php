<?php

namespace App\FrontEnd;

class View
{
	private $basePath;
	private $defaultPage = 'info';
	private $titleDictionary = [];

	public function setBasePath($basePath)
	{
		$this->basePath = $basePath;
	}

	public function setDefaultPage($page)
	{
		$this->defaultPage = $page;
	}

	public function setTitleDictionary(array $titleDictionary)
	{
		$this->titleDictionary = $titleDictionary;
	}

	public function invokeAndReturn()
	{
		ob_start();
		$this->invoke();
		$output = ob_get_clean();
		return $output;
	}

	public function invoke()
	{
		$page = $this->getPage();
		$title = $this->getTitle($page);
		include($this->getPath($page));
	}

	public function getTitle($page)
	{
		return isset($this->titleDictionary[$page]) ? $this->titleDictionary[$page] : 'Untitled';
	}

	public function getPath($filename)
	{
		return $this->basePath . '/' . $filename . '.php';
	}

	private function getPage()
	{
		return $this->getRequestedPage() ?: $this->defaultPage;
	}

	private function getRequestedPage()
	{
		return isset($_GET['page']) ? $_GET['page'] : null;
	}
}