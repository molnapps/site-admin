<?php

namespace App\FrontEnd\ContentProvider;

use \App\FrontEnd\ContentProvider;

class StaffContentProviderHelper
{
	private $contentProvider;

	public function __construct(ContentProvider $contentProvider)
	{
		$this->contentProvider = $contentProvider;
	}

	public function getSortedStaffArray()
	{
		$staffOrder = $this->contentProvider->getPreferences('staffOrder');

		if ( ! $this->isGrouped($staffOrder)) {
			return $this->sortStaffArray($staffOrder);
		}

		$arr = [];
		
		foreach ($staffOrder as $group => $staffOrder) {
			$arr[$group] = $this->sortStaffArray($staffOrder);
		}
		
		return $arr;
	}

	private function isGrouped(array $staffOrder)
	{
		return ! is_numeric(array_keys($staffOrder)[0]);
	}

	private function sortStaffArray(array $staffOrder)
	{
		$collection = $this->contentProvider->getPageBySlug($staffOrder);

		$sortedArray = [];
		
		foreach ($staffOrder as $slug) {
			$sortedArray[] = $collection->filterCollection('slug', [$slug])->getFirst();
		}
		
		return $sortedArray;
	}
}