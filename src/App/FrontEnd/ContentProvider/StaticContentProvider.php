<?php

namespace App\FrontEnd\ContentProvider;

use \Symfony\Component\Yaml\Yaml;
use \App\Domain\Page;
use \App\Domain\Collection;

class StaticContentProvider
{
	private $basePath;
	private $menu = [];

	public function __construct($basePath)
	{
		$this->basePath = $basePath;
		$this->menu = $this->getRawContentForSlug('preferences')['menu'];
	}

	public function getPageBySlug($slugs)
	{
		$rows = [];

		foreach ((array)$slugs as $slug) {
			$rows[] = [
				'title' => isset($this->menu[$slug]) ? $this->menu[$slug] : $this->getTitleFromSlug($slug),
				'slug' => $slug,
				'body' => $this->getContents($slug),
				'heroImage' => '_img/staff/' . $slug . '.jpg',
			];
		}

		$collection = new Collection($rows, new Page);

		return is_array($slugs) ? $collection : $collection->getFirst();
	}

	public function getRawContentForSlug($slug)
	{
		return Yaml::parse($this->getContents($slug));
	}

	private function getTitleFromSlug($slug)
	{
		return ucwords(str_replace('-', ' ', $slug));
	}

	private function getContents($slug)
	{
		return file_get_contents($this->basePath . '/' . $slug . '.txt');
	}
}