<?php

namespace App\FrontEnd;

class Router
{
	private $map = [];

	function register($uri, $page)
	{
		$this->map[$uri] = $page;
	}

	function resolve($uri)
	{
		$result = [];
		
		foreach (array_keys($this->map) as $key) {
			$partsUri = explode('/', $uri);
			$partsMap = explode('/', $key);
			
			if ($this->matches($partsMap, $partsUri)) {
				$result['page'] = $this->map[$key];
				foreach($this->convertWildcards($partsMap, $partsUri) as $wildcard => $value) {
					$result[$wildcard] = $value;
				}
				break;
			}
		}
		
		return $result;
	}

	function matches(array $partsMap, array $partsUri)
	{
		if (count($partsMap) != count($partsUri)) {
			return false;
		}

		foreach ($partsMap as $i => $el) {
			if ( ! $this->isWildcard($el) && $el != $partsUri[$i]) {
				return false;
			}
		}

		return true;
	}

	function convertWildcards($partsMap, $partsUri)
	{
		$result = [];

		foreach ($partsMap as $i => $el) {
			if ($this->isWildcard($el)) {
				$k = $this->cleanWildcard($el);
				$v = $partsUri[$i];
				$result[$k] = $v;
			}
		}

		return $result;
	}

	private function cleanWildcard($el)
	{
		return str_replace(['{', '}'], '', $el);
	}

	private function isWildcard($el) 
	{
		return stripos($el, '{') !== false;
	}
}