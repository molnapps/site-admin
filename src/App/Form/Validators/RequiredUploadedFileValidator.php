<?php

namespace App\Form\Validators;

use App\Control\AdminRequest;

class RequiredUploadedFileValidator extends AbstractValidator
{
	public function validate(AdminRequest $request)
	{
		return ! ($this->rule->isRequiredUploadedFile() && ! $request->hasFile($this->field));
	}

	protected function getMessageIdentifier()
	{
		return 'required';
	}
}