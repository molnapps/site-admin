<?php

namespace App\Form\Validators;

use App\Control\AdminRequest;

class RequiredValidator extends AbstractValidator
{
	public function validate(AdminRequest $request)
	{
		$value = $this->filterRequestValue($this->field, $request);
		return ! ($this->rule->isRequired() && ! $value);
	}

	protected function getMessageIdentifier()
	{
		return 'required';
	}

	private function filterRequestValue($field, $request)
	{
		$this->domainObject->$field = $request->$field;

		return $this->domainObject->$field;
	}
}