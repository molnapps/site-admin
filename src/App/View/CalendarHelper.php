<?php

namespace App\View;

use \Carbon\Carbon;

class CalendarHelper
{
	public static function formatDatetime($datetime, $format = 'd/m/Y')
	{
		return Carbon::createFromFormat('Y-m-d H:i:s', $datetime)->format($format);
	}
}