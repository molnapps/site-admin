<?php

namespace App\View\Attachment;

class AttachmentFactory
{
	private $markup;
	private $text;

	public function __construct($text, $markup)
	{
		$this->markup = $markup ?: '<a href="%s" class="News__attachment">%s</a>';
		$this->text = $text;
	}

	public function createEmbeddableAttachment($attachment)
	{
		$delegates = [
			YoutubeAttachment::class,
		];
		
		foreach ($delegates as $delegateClass) {
			if ($delegateClass::matchesType($attachment)) {
				return $this->createInstance($delegateClass, $attachment);
			}
		}

		return new NullAttachment($attachment);
	}

	public function createAttachment($attachment)
	{
		$delegates = [
			FileAttachment::class,
		];
		
		foreach ($delegates as $delegateClass) {
			if ($delegateClass::matchesType($attachment)) {
				return $this->createInstance($delegateClass, $attachment);
			}
		}

		return new NullAttachment($attachment);
	}

	private function createInstance($delegateClass, $attachment)
	{
		$instance = new $delegateClass($attachment);
		$instance->setMarkup($this->markup);
		$instance->setText($this->text);
		return $instance;
	}
}