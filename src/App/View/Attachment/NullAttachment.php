<?php

namespace App\View\Attachment;

use \App\Domain\Attachment as DomainAttachment;

class NullAttachment extends AbstractAttachment
{
	public static function matchesType(DomainAttachment $attachment)
	{
		return true;
	}

	public function getHtml()
	{
		return '';
	}
}