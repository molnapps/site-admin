<?php

namespace App\View\Attachment;

use \App\Domain\Attachment as DomainAttachment;

interface Attachment
{
	public static function matchesType(DomainAttachment $attachment);
	public function getHtml();
	public function setMarkup($markup);
	public function setText($text);
}