<?php

namespace App\View\Attachment;

use \App\Domain\Attachment as DomainAttachment;

class YoutubeAttachment extends AbstractAttachment
{
	public static function matchesType(DomainAttachment $attachment)
	{
		return stripos($attachment->permalink, 'youtube') !== false;
	}

	public function getHtml()
	{
		return sprintf($this->getEmbedMarkup(), $this->getVideoId());
	}

	private function getEmbedMarkup()
	{
		return '<iframe width="560" height="315" src="https://www.youtube.com/embed/%s" frameborder="0" allowfullscreen></iframe>';
	}

	private function getVideoId()
	{
		$pattern = '%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i';
		preg_match($pattern, $this->attachment->permalink, $match);
		return isset($match[1]) ? $match[1] : null;
	}
}