<?php

namespace App\View\Content;

class NullTag extends Tag
{
	public function build($content = null)
	{
		return '';
	}
}