<?php

namespace App\View\Content;

class Paragraph extends MultilineElement
{
	protected function getLinesSeparator()
	{
		return '<br/>';
	}
}