<?php

namespace App\View\Content;

class Title extends Element
{
	public function matches($line)
	{
		return $this->beginsWithToken($line) && $this->endsWithToken($line);
	}
}