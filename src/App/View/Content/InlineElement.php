<?php

namespace App\View\Content;

class InlineElement extends Element
{
	public function isInline()
	{
		return true;
	}

	public function matches($line)
	{
		return $this->hasAtLeastOneElementWrappedInsideToken($line);
	}

	public function get()
	{
		$lines = $this->getLines();
		
		foreach ($lines as $i => $line) {
			if ( ! $this->hasAtLeastOneElementWrappedInsideToken($line)) {
				continue;
			}

			$parts = $this->getFragments($line);
			
			foreach ($parts as $k => $part) {
				if ( ! $this->isOdd($k)) {
					continue;
				}
				
				if ( ! $this->hasClosingToken($parts, $k)) {
					$parts[$k] = $this->getStartToken() . $part;
					continue;
				}
				
				$parts[$k] = $this->getTag()->build($part);
			}
			
			$lines[$i] = implode('', $parts);
		}

		return implode('', $lines);
	}

	private function hasAtLeastOneElementWrappedInsideToken($line)
	{
		return count($this->getFragments($line)) >= 3;
	}

	private function getFragments($line)
	{
		return explode($this->getStartToken(), $line);
	}
	
	private function isOdd($j)
	{
		return $j % 2 != 0;
	}

	private function hasClosingToken($parts, $k)
	{
		return $k <= count($parts) - 2;
	}
}