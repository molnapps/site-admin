<?php

namespace App\View\Content;

class MultilineElement extends Element
{
	private $lineTag;

	public function __construct(Tag $tag = null, Tag $lineTag = null, $startToken = null, $endToken = null, $canBeNested = false)
	{
		parent::__construct($tag, $startToken, $endToken);
		$this->lineTag = $lineTag;
		$this->canBeNested = $canBeNested;
	}

	public function isMultiline()
	{
		return true;
	}

	public function canBeNested()
	{
		return $this->canBeNested;
	}

	protected function getLineTag()
	{
		return $this->lineTag;
	}
}