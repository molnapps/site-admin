<?php

namespace App\View\Content;

interface FilterInterface
{
	public function get();
	public function addLine($line);
	public function matches($line);
	public function isMultiline();
	public function isInline();
}