<?php

namespace App\View;

class IubendaHelper
{
	private $original;
	private $suppressed;

	public function __construct($attachments)
	{
		$this->original = $attachments;
		$this->suppressed = $attachments;
		
		$this
			->addCssClass()
			->suppressSrc()
			->addDummySrc();
	}

	public function getIframe()
	{
		return $this->suppressed;
	}

	private function addCssClass()
	{
		$this->suppressed = str_replace(
			'<iframe ', 
			'<iframe class="_iub_cs_activate" ', 
			$this->suppressed
		);

		return $this;
	}

	private function suppressSrc()
	{
		$this->suppressed = str_replace(
			'src="', 
			'suppressedsrc="', 
			$this->suppressed
		);

		return $this;
	}

	private function addDummySrc()
	{
		$this->suppressed = str_replace(
			'frameborder', 
			'src="//cdn.iubenda.com/cookie_solution/empty.html" frameborder', 
			$this->suppressed
		);

		return $this;
	}
}