<?php

namespace App\View;

class CookiesHelper
{
	public static function iframe($string)
	{
		return new Iubenda\Iframe($string);
	}

	public static function script($string = null)
	{
		return new Iubenda\Script($string);
	}
}