<?php

namespace App\View;

use \App\Control\AdminRequest;
use \App\Control\AdminView;

use \App\Base\AppContainer;

use \MolnApps\Presenter\HtmlPresenter;

class ViewHelper
{
	private static $instance;

	public static function instance()
	{
		if ( ! static::$instance) {
			static::$instance = new static;
		}

		return static::$instance;
	}

	public static function setInstance(ViewHelper $instance = null)
	{
		static::$instance = $instance;
	}

	public static function __callStatic($name, $args)
	{
		return call_user_func_array([static::instance(), 'get'.ucfirst($name)], $args);
	}

	public function getOld($key, $property = null)
	{
		if ( ! $property) {
			list ($key, $property) = explode('.', $key);
		}

		$domainObject = $this->getRequestObject($key);

		if ($domainObject) {
			return $domainObject->getProperty($property);
		}
	}

	public function getRequestObject($key)
	{
		$rawObject = AdminRequest::instance()->getObject($key);
		if ($rawObject) {
			return new HtmlPresenter($rawObject);
		}
	}

	public function getView($viewName, array $args = [])
	{
		echo (new AdminView([$viewName]))->invoke($args);
	}

	public function getFeedback($type = null)
	{
		$feedbacks = AdminRequest::instance()->getFeedback($type);

		$map = [
			'error' => 'danger',
			'success' => 'success'
		];

		$html = [];

		foreach ($feedbacks as $feedback) {
			$html[] = sprintf('<p class="notification is-%s is-%s">%s</p>', $map[$feedback->type], $feedback->type, $feedback->message);
		}

		return implode("\r\n", $html);
	}

	public function getValidation($identifier)
	{
		$messages = AdminRequest::instance()->getValidationMessages($identifier);
		
		$html = [];
		
		foreach ($messages as $message) {
			$html[] = sprintf(
				'<p class="Validation Validation--error" data-validation-identifier="%s">%s</p>', 
				$message->identifier, $message->body
			);
		}
		
		return implode("\r\n", $html);
	}

	public function getAvailableLanguageField($oldKey)
	{
		$languageProvider = AppContainer::get('languageProvider');
		
		if ($languageProvider->supportsOneLanguage()) {
			return sprintf(
				'<input type="hidden" name="language" value="%s" />', 
				$languageProvider->getFirstAvailableLanguage()
			);
		}

		$html[] = '<label for="language">Lingua</label><br/>';
		$html[] = '<select name="language">';
		$html[] = $this->getOptions($languageProvider->getAvailableLanguages(), $this->getOld($oldKey));
		$html[] = '</select>';

		return implode("\r\n", $html);
	}

	public function getAvailableAuthors()
	{
		$authors = $this->getRequestObject('authorCollection');

		$result = [
			'' => 'None'
		];
		
		foreach ($authors as $author) {
			$result[$author->slug] = $author->getFullName();
		}

		return $result;
	}

	public function getOptions(array $dictionary, $selectedValue = '')
	{
		$html = [];

		foreach ($dictionary as $value => $label) {
			$selected = $value == $selectedValue ? ' selected="selected"' : '';
			$html[] = sprintf('<option value="%s"%s>%s</option>', $value, $selected, $label);
		}

		return implode("\r\n", $html);
	}

	public function getCsrfInput()
	{
		$name = 'token';
		$value = AppContainer::get('session')->csrfToken;
		return sprintf('<input type="hidden" name="%s" value="%s" />', $name, $value);
	}

	public function getMethodInput($value)
	{
		$name = '_method';
		$value = strtoupper($value);
		return sprintf('<input type="hidden" name="%s" value="%s" />', $name, $value);
	}

	public function getEditButton($domainObject, $attr = [])
	{
		if ( ! isset($attr['class'])) {
			$attr['class'] = '';
		}

		if ( ! isset($attr['text'])) {
			$attr['text'] = 'Modifica';
		}
		
		$cmd = 'Edit' . $domainObject->getObjectName();
		$foreignKey = $domainObject->getForeignKey();
		$id = $domainObject->id;

		return sprintf('<a href="index.php?cmd=%s&%s=%s" class="%s">Modifica</a>', $cmd, $foreignKey, $id, $attr['class']);
	}

	public function getTrashButton($domainObject, $attr = [])
	{
		if ( ! isset($attr['class'])) {
			$attr['class'] = '';
		}

		if ( ! isset($attr['text'])) {
			$attr['text'] = 'Elimina';
		}
		
		$cmd = 'ConfirmTrash' . $domainObject->getObjectName();
		$foreignKey = $domainObject->getForeignKey();
		$id = $domainObject->id;

		return sprintf('<a href="index.php?cmd=%s&%s=%s" class="%s">%s</a>', $cmd, $foreignKey, $id, $attr['class'], $attr['text']);
	}

	public function getUnlinkButton($domainObject1, $domainObject2, $cmd, $attr = [])
	{
		if ( ! isset($attr['class'])) {
			$attr['class'] = '';
		}

		if ( ! isset($attr['text'])) {
			$attr['text'] = 'Rimuovi';
		}
		
		$foreignKey1 = $domainObject1->getPrimaryKey();
		$id1 = $domainObject1->id;

		$foreignKey2 = $domainObject2->getForeignKey();
		$id2 = $domainObject2->id;

		return sprintf('<a href="index.php?cmd=%s&%s=%s&%s=%s" class="%s">%s</a>', $cmd, $foreignKey1, $id1, $foreignKey2, $id2, $attr['class'], $attr['text']);
	}

	public function getUnlinkConfirmButton($domainObject1, $domainObject2, $cmd)
	{
		return sprintf('
			<form method="POST" action="index.php" id="unlinkForm">
				<input type="submit" value="Elimina" class="button is-warning is-inverted" />
				<input type="hidden" name="cmd" value="%s" />
				<input type="hidden" name="%s" value="%s" />
				<input type="hidden" name="%s" value="%s" />
				%s
				%s
			</form>', 
			$cmd, 
			$domainObject1->getPrimaryKey(), 
			$domainObject1->id,
			$domainObject2->getForeignKey(), 
			$domainObject2->id,
			$this->getCsrfInput(),
			$this->getMethodInput('POST')
		);
	}

	public function getTrashConfirmButton($domainObject)
	{
		$cmd = 'Trash' . $domainObject->getObjectName();
		return sprintf('
			<form method="POST" action="index.php" id="trashForm">
				<input type="submit" value="Elimina" class="button is-warning is-inverted" />
				<input type="hidden" name="cmd" value="%s" />
				<input type="hidden" name="%s" value="%s" />
				%s
				%s
			</form>', 
			$cmd, 
			$domainObject->getForeignKey(), 
			$domainObject->id,
			$this->getCsrfInput(),
			$this->getMethodInput('POST')
		);
	}
}