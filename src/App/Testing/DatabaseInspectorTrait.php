<?php

namespace App\Testing;

trait DatabaseInspectorTrait
{
	public function assertObjectCount($targetClass, $count)
	{
		$collection = (new $targetClass)->find();
		$this->assertCount($count, $collection, 'Stored objects count mismatch');
	}

	public function assertAvailableObjectCount($targetClass, $count)
	{
		$collection = $targetClass::available()->find();
		
		$this->assertCount($count, $collection, 'Available stored objects count mismatch');
	}

	public function assertTrashedObjectCount($targetClass, $count)
	{
		$collection = $targetClass::trashed()->find();

		$this->assertCount($count, $collection, 'Trashed stored objects count mismatch');
	}

	public function assertObjectMatches($targetClass, $index, array $properties = [])
	{
		$collection = (new $targetClass)->find();
		$this->assertNotNull($collection);
		$this->assertFalse($collection->isEmpty());
		$domainObject = $collection->getObject($index);
		foreach ($properties as $key => $value) {
			$this->assertEquals($value, $domainObject->$key, 'Stored object value mismatch: ' . $key);
		}
	}
}