<?php

namespace App\Testing;

use \MolnApps\Testing\Response\ResponseInspectorTrait;

use \PHPUnit\Framework\TestCase;

class AdminTestCase extends TestCase
{
    use InteractsWithDatabaseTrait;
    use InteractsWithAppTrait;
    use ResponseInspectorTrait;
    use DatabaseInspectorTrait;
    use SeeResponseTrait;
    use FactoryTrait;
    use InteractsWithFileSystem;
    use InteractsWithTrashConfirmationScreen;

    protected function signedIn()
    {
        return $this->signedInAs(null);
    }

    protected function visitPost($command, array $params = [])
    {
        $params['_method'] = 'POST';
        return $this->visit($command, $params);
    }
}