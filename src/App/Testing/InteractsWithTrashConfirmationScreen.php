<?php

namespace App\Testing;

trait InteractsWithTrashConfirmationScreen
{
	protected function attemptToTrash($domainObject)
	{
		return $this
			->clickTrashButton($domainObject)
			->seeTrashConfirmationScreen($domainObject)
			->clickTrashConfirmButton();
	}

	protected function attemptToTrashButCancel($domainObject)
	{
		return $this
			->clickTrashButton($domainObject)
			->seeTrashConfirmationScreen($domainObject)
			->clickTrashCancelButton();
	}

	protected function clickTrashButton($domainObject)
	{
		$name = lcfirst($domainObject->getObjectName());

		return $this
			->seeElement('div.' . $name . '-' . $domainObject->id)
			->click(['Elimina', '.card-footer-item']);
	}

	protected function seeTrashConfirmationScreen($domainObject)
	{
		return $this
			->seeForm('trashForm')
			->seeInput($domainObject->getForeignKey())
				->withValue($domainObject->id);
	}

	protected function clickTrashConfirmButton()
	{
		return $this
			->seeForm('trashForm')
			->submit();
	}

	protected function clickTrashCancelButton()
	{
		return $this->click('Annulla');
	}
}