<?php

use Phinx\Migration\AbstractMigration;

class AddDisplayDateTimeToNewsTable extends AbstractMigration
{
    public function change()
    {
        $table = $this->table('news');

        $table->addColumn('display_date', 'datetime', ['null' => true]);

        $table->update();
    }
}
