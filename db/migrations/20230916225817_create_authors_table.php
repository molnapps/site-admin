<?php

use Phinx\Migration\AbstractMigration;

class CreateAuthorsTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this->table('authors');

        $table
            ->addColumn('slug', 'string')
        	->addColumn('title', 'string', ['null' => true])
            ->addColumn('first_name', 'string')
            ->addColumn('last_name', 'string')
            ->addColumn('company', 'string', ['null' => true]);

        $table->addTimestamps();
        $table->addColumn('deleted_at', 'timestamp', ['null' => true]);
        $table->addColumn('published_at', 'timestamp', ['null' => true]);

        $table->create();
    }
}
