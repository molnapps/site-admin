<?php
use \App\View\ViewHelper;
?>
<section class="section">
	<div class="container">
		<h2 class="title">Allegato</h2>
		<?= ViewHelper::feedback(); ?>
		<form method="post" action="index.php?cmd=SaveAttachment" enctype="multipart/form-data" class="Form" id="attachmentForm">
			<!-- Attachment Title -->
			<div class="field">
				<label class="label" for="title">Titolo</label>
				<div class="control">
					<input class="input" type="text" name="title" id="title" value="<?= ViewHelper::old('attachment.title'); ?>" />
				</div>
				<?= ViewHelper::validation('attachment.title'); ?>
			</div>
			<!-- Attachment -->
			<div class="field">
				<label class="label" for="url">Url</label>
				<div class="control">
					<input class="input" type="text" name="url" id="url" placeholder="http://" value="" />
				</div>
			</div>
			<div class="field">
				<label class="label" for="attachment">Allegato</label>
				<div class="control">
					<input type="file" name="attachment" id="attachment" />
				</div>
				<?= ViewHelper::validation('attachment.attachment'); ?>
			</div>
			<!-- Actions -->
			<div class="field is-grouped">
				<div class="control">
					<input type="submit" value="Salva" class="button is-primary" />
				</div>
				<div class="control">
					<a href="index.php?cmd=EditNews&news_id=<?= ViewHelper::old('news.id'); ?>" class="button is-secondary">Annulla</a>
				</div>
			</div>
			<?= ViewHelper::csrfInput(); ?>
			<?= ViewHelper::methodInput('POST'); ?>
			<input type="hidden" name="news_id" value="<?= ViewHelper::old('news.id'); ?>" />
			<input type="hidden" name="id" value="<?= ViewHelper::old('attachment.id'); ?>" />
		</form>
	</div>
</section>