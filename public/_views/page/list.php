<?php

use \App\View\ViewHelper;
use \App\Base\AppContainer;
use \Carbon\Carbon;

$pageCollection = ViewHelper::requestObject('pageCollection');
?>
<section class="section">
	<div class="container">
		<?= ViewHelper::feedback(); ?>
		<h2 class="title">Pagine</h2>
		<nav class="level">
			<!-- Left side -->
			<div class="level-left">
				<p class="level-item"><a class="button is-success" href="index.php?cmd=NewPage">Nuova pagina</a></p>
			</div>
		</nav>
		<?php
		foreach ($pageCollection as $page) {
			extract([
				'title' => $page->title,
				'formattedDate' => Carbon::parse($page->created_at)->formatLocalized('%d %B %Y'),
				'status' => ($page->isPublished() ? 'Pubblicato' : 'Bozza'),
				'language' => AppContainer::get('languageProvider')->getLanguageName($page->language),
			]);
			include('card.partial.php');
		}
		?>
	</div>
</section>