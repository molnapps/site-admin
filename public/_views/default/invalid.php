<?php
use \App\View\ViewHelper;
?>
<section class="hero is-danger">
	<div class="hero-body">
		<div class="container">
			<h2 class="title">Invalid request</h2>
			<?= ViewHelper::feedback(); ?>
		</div>
	</div>
</section>