<?php
use \App\View\ViewHelper;
?>
<section class="hero is-warning">
	<div class="hero-body">
		<div class="container">
			<h2 class="title"><?= $title; ?></h2>
			<div class="content">
				<p><?= $message; ?></p>
			</div>
			<div class="field is-grouped">
				<div class="control">
					<?= ViewHelper::trashConfirmButton($domainObject); ?>
				</div>
				<div class="control">
					<a href="index.php?cmd=<?= $cancelCommand; ?>" class="button is-warning is-inverted is-outlined">Annulla</a>
				</div>
			</div>
		</div>
	</div>
</section>
