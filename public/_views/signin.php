<?php
use \App\View\ViewHelper;
?>
<section class="hero is-primary">
	<div class="hero-body">
		<div class="columns">
			<div class="column is-one-third">
				<h2 class="title">Sign in</h2>
				<form action="index.php?cmd=SignIn" method="post" class="Form Form--hinted" id="signInForm">
					<div class="field">
						<div class="control">
							<input class="input" type="text" name="username" value="" placeholder="Username" />
						</div>
					</div>
					<div class="field">
						<div class="control">
							<input class="input" type="password" name="password" value="" placeholder="Password" /><br/>
						</div>
					</div>
					<div class="field is-grouped">
						<div class="control">
							<input class="button is-primary is-inverted is-outlined" type="submit" value="Sign in" /><br/>
						</div>
					</div>
					<input type="hidden" name="_method" value="post" />
					<?= ViewHelper::csrfInput(); ?>
					<?= ViewHelper::methodInput('POST'); ?>
				</form>
			</div>
		</div>
	</div>
</section>