<div class="field">
	<label class="label" for=""><?= $label; ?></label>
	<div class="control">
		<div class="select">
			<select class="select" name="publishDate_dd">
				<option value="">—</option>
				<?php
				for($i = 1; $i <= 31; $i++) {
					$selected = $dd == $i ? ' selected="selected"' : '';
					echo sprintf('<option value="%s"%s>%s</option>', $i, $selected, $i);
				}
				?>
			</select>
		</div>
		<div class="select">
			<select class="select" name="publishDate_mm">
				<option value="">—</option>
				<?php
				foreach(['Gen', 'Feb', 'Mar', 'Apr', 'Mag', 'Giu', 'Lug', 'Ago', 'Set', 'Ott', 'Nov', 'Dic'] as $i => $month) {
					$selected = $mm == ($i + 1) ? ' selected="selected"' : '';
					echo sprintf('<option value="%s"%s>%s</option>', ($i + 1), $selected, $month);
				}
				?>
			</select>
		</div>
		<div class="select">
			<select class="select" name="publishDate_yy">
				<option value="">—</option>
				<?php
				$currentYear = gmdate('Y');
				$startYear = $currentYear - 10;
				$endYear = $currentYear + 10;
				$startYear = ($yy && $yy < $startYear) ? $yy : $startYear;
				$years = range($startYear, $endYear);
				foreach($years as $year) {
					$selected = ($yy == $year) ? ' selected="selected"' : '';
					echo sprintf('<option value="%s"%s>%s</option>', $year, $selected, $year);
				}
				?>
			</select>
		</div>
	</div>
</div>