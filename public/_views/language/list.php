<?php

use \App\Base\AppContainer;

$languageProvider = AppContainer::get('languageProvider');
?>
<section class="hero is-primary">
	<div class="hero-body">
		<div class="container">
			<h2 class="title">Lingua</h2>
		</div>
	</div>
</section>
<section class="section">
	<div class="container">
		<div class="content">
			<p>Scegli la lingua di cui vorresti modificare i contenuti:</p>
		</div>
		<nav class="panel">
			<p class="panel-heading">Lingue supportate</p>
			<?php
			foreach ($languageProvider->getAvailableLanguages() as $key => $language) {
				$isActive = $key == $languageProvider->getCurrentLanguage() ? 'is-active' : '';
				echo sprintf('<a class="panel-block %s" href="index.php?cmd=SetLanguage&language=%s">%s</a>', $isActive, $key, $language);
			}
			?>
		</nav>
	</div>
</section>