<!DOCTYPE html>
<html lang="it">
<head>
	<meta charset="UTF-8">
	<title>Site Admin</title>
	<link rel="stylesheet" href="_css/admin.build.css">
</head>
<body>
	<nav class="navbar ">
		<div class="navbar-brand">
			<a class="navbar-item" href="index.php"><strong>Site Admin</strong></a>
			<div class="navbar-burger burger" data-target="navMenuExample">
		      <span></span>
		      <span></span>
		      <span></span>
		    </div>
		</div>
		<div class="navbar-menu">
			<div class="navbar-start">
				<a class="navbar-item" href="index.php?cmd=ListNews">News &amp; Eventi</a>
				<a class="navbar-item" href="index.php?cmd=ListPage">Pagine</a>
			</div>
			<div class="navbar-end">
				<?php
				$currentLanguage = \App\Base\AppContainer::get('languageProvider')->getCurrentLanguageName();
				if ($currentLanguage) {
					echo sprintf('<a href="index.php?cmd=SetLanguage" class="navbar-item">%s</a>', $currentLanguage);
				}
				?>
				<a class="navbar-item" href="index.php?cmd=SignOut">Sign out</a>
			</div>
		</div>
	</nav>
	