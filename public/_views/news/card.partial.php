<?php
use \App\View\ViewHelper;
?>
<div class="card card--news news-<?= $news->id ?>">
	<header class="card-header">
		<p class="card-header-title"><?= $subject; ?></p>
	</header>
	<div class="card-content">
		<div class="content">
			<p><?= $excerpt; ?></p>
			<p>
				<strong>Data: </strong><?= $formattedDate; ?><br/>
				<strong>Allegati: </strong><?= $attachmentsCount; ?><br/>
				<strong>Stato: </strong><?= $status; ?><br/>
				<strong>Lingua: </strong><?= $language; ?><br/>
			</p>
		</div>
	</div>
	<footer class="card-footer">
		<?= ViewHelper::editButton($news, ['class' => 'card-footer-item']); ?>
	    <?= ViewHelper::trashButton($news, ['class' => 'card-footer-item']); ?>
	</footer>
</div>
<hr/>