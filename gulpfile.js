var gulp = require('gulp');
var watch = require('gulp-watch');
var sass = require('gulp-sass');
var sourcemaps = require('gulp-sourcemaps');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var notify = require('gulp-notify');

gulp.task('sass', function() {
	return gulp.src([
  			'public/_sass/admin.build.scss'
 		])
		.pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
		.pipe(gulp.dest('./public/_css'))
		.pipe(notify({ message: 'Admin Sass compiled.' }));;
});

gulp.task('js', function() {
	return;
	/*
	return gulp.src([
			'public/_js/parallax.js',
			'public/_js/script.js',
		])
		// Init maps
		.pipe(sourcemaps.init())
		// Concat
		.pipe(concat('build.min.js'))
		// Minify
		.pipe(uglify())
		// Write maps
		.pipe(sourcemaps.write('.', {
			includeContent: false,
	    }))
		.pipe(gulp.dest('./public/_js'))
		.pipe(notify({ message: 'Javascript compiled.' }));
	*/
});

// configure which files to watch and what tasks to use on file changes
gulp.task('watch', function() {
	gulp.watch('public/admin/_sass/**/*.scss', ['sass']);
	gulp.watch('public/_js/**/*.js', ['js']);
});

gulp.task('default', ['sass', 'js']);