<?php

namespace Tests\Unit\App\Domain;

use \App\Testing\AdminTestCase;
use \App\Domain\Author;

class AuthorTest extends AdminTestCase
{
	/** @test */
	public function it_can_be_instantiated()
	{
		$author = new Author;

		$this->assertNotNull($author);
	}

    /** @test */
	public function it_can_be_instantiated_with_properties()
	{
		$author = new Author([
			'id' => 1,
			'created_at' => '2015-02-18 02:03:03',
			'updated_at' => '2015-02-18 02:03:03',
			'deleted_at' => null,
            'slug' => 'john-doe',
			'title' => 'Dott.',
			'first_name' => 'John',
            'last_name' => 'Doe',
            'company' => 'Studio Doe'
		]);

		$this->assertEquals(1, $author->id);
		$this->assertEquals('2015-02-18 02:03:03', $author->created_at);
		$this->assertEquals('2015-02-18 02:03:03', $author->updated_at);
		$this->assertEquals(null, $author->deleted_at);
		$this->assertEquals('Dott.', $author->title);
		$this->assertEquals('John', $author->first_name);
        $this->assertEquals('Doe', $author->last_name);
        $this->assertEquals('Studio Doe', $author->company);
	}

    /** @test */
	public function it_accepts_properties()
	{
		$author = new Author;

		$author->id = 1;
		$author->created_at = '2015-02-18 02:03:03';
		$author->updated_at = '2015-02-18 02:03:03';
		$author->deleted_at = null;
        $author->slug = 'john-doe';
		$author->title = 'Dott.';
		$author->first_name = 'John';
        $author->last_name = 'Doe';
        $author->company = 'Studio Doe';

		$this->assertEquals(1, $author->id);
		$this->assertEquals('2015-02-18 02:03:03', $author->created_at);
		$this->assertEquals('2015-02-18 02:03:03', $author->updated_at);
		$this->assertEquals(null, $author->deleted_at);
		$this->assertEquals('Dott.', $author->title);
        $this->assertEquals('John', $author->first_name);
        $this->assertEquals('Doe', $author->last_name);
        $this->assertEquals('Studio Doe', $author->company);
	}

    /** @test */
	public function it_can_be_saved()
	{
		$author = new Author([
			'slug' => 'john-doe',
            'title' => 'Dott.',
			'first_name' => 'John',
			'last_name' => 'Doe',
            'company' => 'Studio Doe',
		]);

		$this->assertNull($author->created_at);

		$author->save();

		$this->assertNotNull($author->created_at);

		$this->assertEquals(1, $author->id);		
	}

    /** @test */
	public function it_can_be_updated()
	{
		$author = new Author([
			'id' => 1,
			'created_at' => '2015-02-18 02:03:03',
			'slug' => 'john-doe',
            'title' => 'Dott.',
			'first_name' => 'John',
			'last_name' => 'Doe',
            'company' => 'Studio Doe',
		]);

        $this->assertNull($author->updated_at);

        $author->save();

		$this->assertNotNull($author->updated_at);
	}

    /** @test */
	public function it_can_be_trashed()
	{
		$author = new Author([
			'id' => 1,
			'created_at' => '2015-02-18 02:03:03',
			'slug' => 'john-doe',
            'title' => 'Dott.',
			'first_name' => 'John',
			'last_name' => 'Doe',
            'company' => 'Studio Doe',
		]);

		$this->assertNull($author->deleted_at);

		$author->trash();

		$this->assertNotNull($author->deleted_at);	
	}

    /** @test */
	public function it_can_be_restored()
	{
		$author = new Author([
			'id' => 1,
			'created_at' => '2015-02-18 02:03:03',
			'deleted_at' => '2015-02-18 02:03:03',
			'slug' => 'john-doe',
            'title' => 'Dott.',
			'first_name' => 'John',
			'last_name' => 'Doe',
            'company' => 'Studio Doe',
		]);

		$this->assertNotNull($author->deleted_at);

		$author->restore();

		$this->assertNull($author->deleted_at);	
	}

    /** @test */
	public function it_can_be_saved_without_company()
	{
		$author = new Author([
			'slug' => 'john-doe',
            'title' => 'Dott.',
			'first_name' => 'John',
			'last_name' => 'Doe',
            'company' => null,
		]);

		$this->assertNull($author->created_at);

		$author->save();

		$this->assertNotNull($author->created_at);

		$this->assertEquals(1, $author->id);		
	}

    /** @test */
	public function it_returns_full_name()
	{
		$author = new Author([
			'slug' => 'john-doe',
            'title' => 'Dott.',
			'first_name' => 'John',
			'last_name' => 'Doe',
            'company' => null,
		]);

		$this->assertEquals('Dott. John Doe', $author->getFullName());
	}

    /** @test */
	public function it_returns_full_name_without_title()
	{
		$author = new Author([
			'slug' => 'john-doe',
            'title' => null,
			'first_name' => 'John',
			'last_name' => 'Doe',
            'company' => null,
		]);

		$this->assertEquals('John Doe', $author->getFullName());
	}

    /** @test */
	public function it_returns_full_name_without_last_name()
	{
		$author = new Author([
			'slug' => 'john-doe',
            'title' => 'Dott.',
			'first_name' => 'John',
			'last_name' => null,
            'company' => null,
		]);

		$this->assertEquals('Dott. John', $author->getFullName());
	}
}