<?php

namespace Tests\Unit\App\Domain;

use \App\Testing\AdminTestCase;
use \App\Domain\News;
use \App\Domain\Author;

class NewsTest extends AdminTestCase
{
	/** @test */
	public function it_can_be_instantiated()
	{
		$news = new News;

		$this->assertNotNull($news);
	}

	/** @test */
	public function it_can_be_instantiated_with_properties()
	{
		$news = new News([
			'id' => 1,
			'created_at' => '2015-02-18 02:03:03',
			'updated_at' => '2015-02-18 02:03:03',
			'deleted_at' => null,
			'body' => 'Lorem ipsum',
			'attachment_id' => null,
		]);

		$this->assertEquals(1, $news->id);
		$this->assertEquals('2015-02-18 02:03:03', $news->created_at);
		$this->assertEquals('2015-02-18 02:03:03', $news->updated_at);
		$this->assertEquals(null, $news->deleted_at);
		$this->assertEquals('Lorem ipsum', $news->body);
		$this->assertNull($news->attachment_id);
	}

	/** @test */
	public function it_accepts_properties()
	{
		$news = new News;

		$news->id = 1;
		$news->created_at = '2015-02-18 02:03:03';
		$news->updated_at = '2015-02-18 02:03:03';
		$news->deleted_at = null;
		$news->body = 'Lorem ipsum';
		$news->attachment_id = null;

		$this->assertEquals(1, $news->id);
		$this->assertEquals('2015-02-18 02:03:03', $news->created_at);
		$this->assertEquals('2015-02-18 02:03:03', $news->updated_at);
		$this->assertEquals(null, $news->deleted_at);
		$this->assertEquals('Lorem ipsum', $news->body);
		$this->assertNull($news->attachment_id);
	}

	/** @test */
	public function it_accepts_multiple_properties()
	{
		$news = new News;

		$news->setProperties([
			'id' => 1,
			'created_at' => '2015-02-18 02:03:03',
			'updated_at' => '2015-02-18 02:03:03',
			'deleted_at' => null,
			'body' => 'Lorem ipsum',
			'attachment_id' => null,
		]);

		$this->assertEquals(1, $news->id);
		$this->assertEquals('2015-02-18 02:03:03', $news->created_at);
		$this->assertEquals('2015-02-18 02:03:03', $news->updated_at);
		$this->assertEquals(null, $news->deleted_at);
		$this->assertEquals('Lorem ipsum', $news->body);
		$this->assertNull($news->attachment_id);
	}

	/** @test */
	public function it_marks_a_property_as_dirty()
	{
		$news = new News([
			'id' => 1,
			'created_at' => '2015-02-18 02:03:03',
			'updated_at' => '2015-02-18 02:03:03',
			'deleted_at' => null,
			'body' => 'Lorem ipsum',
			'attachment_id' => null,
		]);

		$this->assertFalse($news->isDirty('body'));

		$news->body = 'Dolor sit amet';

		$this->assertTrue($news->isDirty('body'));
	}

	/** @test */
	public function it_implements_requestable()
	{
		$news = new News;

		$this->assertEquals('News', $news->getObjectName());
	}

	/** @test */
	public function it_can_be_saved()
	{
		$news = new News([
			'language' => 'it_IT',
			'type' => 'news',
			'subject' => 'My title',
			'body' => 'Lorem ipsum',
			'attachment_id' => null,
		]);

		$this->assertNull($news->created_at);

		$news->save();

		$this->assertNotNull($news->created_at);

		$this->assertEquals(1, $news->id);		
	}

	/** @test */
	public function it_can_be_updated()
	{
		$news = new News([
			'id' => 1,
			'created_at' => '2015-02-18 02:03:03',
			'body' => 'Lorem ipsum',
			'attachment_id' => null,
		]);

		$this->assertNull($news->updated_at);

		$news->save();

		$this->assertNotNull($news->updated_at);
	}

	/** @test */
	public function it_can_be_trashed()
	{
		$news = new News([
			'id' => 1,
			'created_at' => '2015-02-18 02:03:03',
			'body' => 'Lorem ipsum',
			'attachment_id' => null,
		]);

		$this->assertNull($news->deleted_at);

		$news->trash();

		$this->assertNotNull($news->deleted_at);	
	}

	/** @test */
	public function it_can_be_restored()
	{
		$news = new News([
			'id' => 1,
			'created_at' => '2015-02-18 02:03:03',
			'deleted_at' => '2015-02-18 02:03:03',
			'body' => 'Lorem ipsum',
			'attachment_id' => null,
		]);

		$this->assertNotNull($news->deleted_at);

		$news->restore();

		$this->assertNull($news->deleted_at);	
	}

	/** @test */
	public function it_returns_the_upcoming_events_count()
	{
		// Assuming I have no upcoming events
		$this->assertEquals(0, News::upcomingEventsCount());

		// And I have a news
		$today = gmdate('Y-m-d H:i:s', time() + 60);
		$this->persist(News::class, ['type' => 'news', 'display_date' => $today]);

		// Assuming I have two upcoming events
		$tomorrow = gmdate('Y-m-d H:i:s', strtotime('tomorrow'));
		$this->persist(News::class, ['type' => 'event', 'display_date' => $tomorrow]);
		$this->persist(News::class, ['type' => 'event', 'display_date' => $tomorrow]);

		// And I have one event later today
		$today = gmdate('Y-m-d H:i:s', time() + 60);
		$this->persist(News::class, ['type' => 'event', 'display_date' => $today]);

		// And I have another event today, but past
		$today = gmdate('Y-m-d H:i:s', time() - 60);
		$this->persist(News::class, ['type' => 'event', 'display_date' => $today]);
		
		// And a past event
		$yesterday = gmdate('Y-m-d H:i:s', strtotime('yesterday'));
		$this->persist(News::class, ['type' => 'event', 'display_date' => $yesterday]);

		// Then it should return 2
		$this->assertEquals(3, News::upcomingEventsCount());
	}

	/** @test */
	public function it_returns_excerpt()
	{
		$news = new News([
			'body' => 'Lorem ipsum dolor sit amet',
		]);

		$this->assertEquals('Lorem ipsum...', $news->getExcerpt(15));

		$this->assertFalse($news->hasExcerpt(100));
		$this->assertTrue($news->hasExcerpt(15));
	}

	/** @test */
	public function it_returns_no_author_by_default()
	{
		$news = $this->persist(News::class);

		$this->assertCount(0, $news->authors);
	}

	/** @test */
	public function it_returns_author()
	{
		$news = $this->persist(News::class);
		$author = $this->persist(Author::class);

		$news->authors()->attach($author);

		$this->assertCount(1, $news->authors);
		$this->assertEquals(
			'Dr. John Doe', 
			$news->authors->getFirst()->getFullName()
		);
	}
}