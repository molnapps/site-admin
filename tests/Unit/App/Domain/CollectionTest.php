<?php

namespace Tests\Unit\App\Domain;

use \PHPUnit\Framework\TestCase;

use \App\Domain\Collection;
use \App\Domain\News;

class CollectionTest extends TestCase
{
	/** @test */
	public function it_can_be_instantiated()
	{
		$collection = new Collection;

		$this->assertNotNull($collection);
	}

	/** @test */
	public function it_accepts_some_rows()
	{
		$collection = new Collection([
			['id' => 1, 'body' => 'lorem ipsum']
		]);

		$this->assertCount(1, $collection);
	}

	/** @test */
	public function it_implements_iterator()
	{
		$collection = new Collection([
			['id' => 1, 'body' => 'lorem ipsum'],
		], new News);

		foreach ($collection as $news) {
			$this->assertInstanceOf(News::class, $news);
		}
	}

	/** @test */
	public function it_implements_requestable()
	{
		$collection = new Collection([], null, new News);

		$this->assertEquals('NewsCollection', $collection->getObjectName());
	}
}