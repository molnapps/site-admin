<?php

namespace Tests\Unit\App\Api\Resources;

use \App\Testing\AdminTestCase;
use \App\Api\NewsResponseFactory;
use \App\Api\ResponseData;
use \App\Domain\News;
use \Carbon\Carbon;

class NewsResponseFactoryTest extends AdminTestCase
{
    /** @test */
	public function it_can_be_instantiated()
	{
        $this->assertNotNull(new NewsResponseFactory);
	}

    /** @test */
	public function it_returns_respnse_object()
	{
        $result = (new NewsResponseFactory)->getAllPublished();

        $this->assertInstanceOf(ResponseData::class, $result);
	}

    /** @test */
	public function it_returns_no_news_if_database_is_empty()
	{
        $result = (new NewsResponseFactory)->getAllPublished();

        $this->assertCount(0, $result->toArray());
	}

    /** @test */
	public function it_returns_news_if_database_is_not_empty_but_news_is_not_published()
	{
        $this->persist(News::class);

        $result = (new NewsResponseFactory)->getAllPublished();

        $this->assertCount(0, $result->toArray());
	}

    /** @test */
	public function it_returns_news_if_database_is_not_empty()
	{
        $this->persist(News::class, [
            'published_at' => Carbon::yesterday()
        ]);

        $result = (new NewsResponseFactory)->getAllPublished();

        $this->assertCount(1, $result->toArray());
	}

    /** @test */
	public function it_returns_multiple_news_if_database_is_not_empty()
	{
        $this->persist(News::class, [
            'published_at' => Carbon::yesterday()
        ]);

        $this->persist(News::class, [
            'published_at' => Carbon::yesterday()
        ]);

        $result = (new NewsResponseFactory)->getAllPublished();

        $this->assertCount(2, $result->toArray());
	}

    /** @test */
	public function it_returns_all_type_of_news()
	{
        $this->persist(News::class, [
            'type' => 'news',
            'published_at' => Carbon::yesterday()
        ]);

        $this->persist(News::class, [
            'type' => 'event',
            'published_at' => Carbon::yesterday()
        ]);

        $result = (new NewsResponseFactory)->getAllPublished();

        $this->assertCount(2, $result->toArray());
	}

    /** @test */
	public function it_returns_news_within_limit_if_database_is_not_empty()
	{
        $this->persist(News::class, [
            'published_at' => Carbon::yesterday()
        ]);

        $this->persist(News::class, [
            'published_at' => Carbon::yesterday()
        ]);

        $result = (new NewsResponseFactory)
            ->getAllPublished($limit = 1);

        $this->assertCount(1, $result->toArray());
	}

    /** @test */
	public function it_returns_news_within_limit_for_each_type()
	{
        $this->persist(News::class, [
            'type' => 'news',
            'published_at' => Carbon::yesterday()
        ]);

        $this->persist(News::class, [
            'type' => 'news',
            'published_at' => Carbon::yesterday()
        ]);

        $this->persist(News::class, [
            'type' => 'event',
            'published_at' => Carbon::yesterday()
        ]);

        $this->persist(News::class, [
            'type' => 'event',
            'published_at' => Carbon::yesterday()
        ]);

        $instance = new NewsResponseFactory;

        $this->assertCount(4, $instance->getAllPublished()->toArray());
        $this->assertCount(2, $instance->getAllPublished($limit = 1)->toArray());
	}

    /** @test */
	public function it_returns_resource_as_array()
	{
        $this->persist(News::class, [
            'published_at' => Carbon::yesterday()
        ]);

        $result = (new NewsResponseFactory)->getAllPublished();

        $this->assertCount(1, $result->toArray());
        $this->assertIsArray($result->toArray()[0]);
	}
    
    /** @test */
	public function it_provide_type_constatns()
	{
        $this->assertEquals(null, NewsResponseFactory::TYPES_ALL);
        $this->assertEquals('news', NewsResponseFactory::TYPES_NEWS);
        $this->assertEquals('event', NewsResponseFactory::TYPES_EVENTS);
	}
}