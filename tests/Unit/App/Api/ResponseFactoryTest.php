<?php

namespace Tests\Unit\App\Api;

use \App\Testing\AdminTestCase;
use \App\Api\ResponseFactory;
use \App\Api\NewsResponseFactory;
use \App\Domain\News as DomainNews;

class ResponseFactoryTest extends AdminTestCase
{
    /** @test */
	public function it_creates_a_news_response_factory()
	{
        $this->assertInstanceOf(
            NewsResponseFactory::class, 
            ResponseFactory::make(DomainNews::class)
        );
	}

    /** @test */
	public function it_throws_an_exception_if_key_is_not_found()
	{
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('There is no response factory for key [foobar]');

        ResponseFactory::make('foobar');
	}
}