<?php

namespace Tests\Unit\App\Api;

use \App\Testing\AdminTestCase;
use \App\Api\ResponseData;
use \App\Api\ApiResponse;
use \App\Api\Resources\News as NewsResource;
use \App\Api\Contracts\Jsonable;
use \App\Api\Contracts\Arrayable;
use \App\Domain\News;
use \Carbon\Carbon;

class ResponseDataTest extends AdminTestCase
{
    /** @test */
	public function it_implements_arrayable_interface()
	{
        $result = new ResponseData([]);

        $this->assertInstanceOf(Arrayable::class, $result);
	}

    /** @test */
	public function it_returns_array_for_empty_array()
	{
        $instance = new ResponseData([]);

        $this->assertEquals([], $instance->toArray());
	}

    /** @test */
	public function it_returns_array_for_array_item()
	{
        $instance = new ResponseData($expected = [
            ['foo']
        ]);

        $this->assertEquals(
            $expected, 
            $instance->toArray()
        );
	}

    /** @test */
	public function it_returns_array_for_arrayable_item()
	{
        $news = $this->persist(News::class);
        $arrayable = new NewsResource($news);

        $this->assertInstanceOf(Arrayable::class, $arrayable);

        $instance = new ResponseData([$arrayable]);

        $this->assertEquals([$arrayable->toArray()], $instance->toArray());
	}


    /** @test */
	public function throws_an_exception_for_invalid_item()
	{
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('$resource should either be a Resource instance or an array');

        $instance = new ResponseData([
            'foo'
        ]);

        $this->assertEquals([], $instance->toArray());
	}

    /** @test */
	public function it_returns_api_response_instance()
	{
        $instance = new ResponseData([
            'foo'
        ]);

        $this->assertInstanceOf(ApiResponse::class, $instance->toApiResponse());
	}
}