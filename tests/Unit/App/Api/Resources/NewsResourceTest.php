<?php

namespace Tests\Unit\App\Api\Resources;

use \App\Testing\AdminTestCase;
use \App\Api\Resources\News;
use \App\Api\Contracts\Jsonable;
use \App\Api\Contracts\Arrayable;
use \App\Domain\News as DomainNews;
use \App\Domain\Author as DomainAuthor;
use \App\Domain\Attachment as DomainAttachment;

class NewsResourceTest extends AdminTestCase
{
	/** @test */
	public function it_can_be_instantiated()
	{
		$newsResource = new News();

		$this->assertNotNull($newsResource);
	}

    /** @test */
	public function it_implements_jsonable_interface()
	{
        $newsResource = new News();

		$this->assertInstanceOf(Jsonable::class, $newsResource);
    }

    /** @test */
	public function it_implements_arrayable_interface()
	{
        $newsResource = new News();

		$this->assertInstanceOf(Arrayable::class, $newsResource);
    }

    /** @test */
	public function it_returns_array()
	{
        $news = $this->persist(DomainNews::class, [
            'type' => 'news', 
            'subject' => 'Foo bar', 
            'body' => 'Lorem ipsum dolor',
            'display_date' => '2017-05-12 00:00:00',
        ]);

        $newsResource = new News($news);

        $result = $newsResource->toArray();

        $this->assertArrayHasKey('id', $result);
        $this->assertEquals($news->id, $result['id']);

        $this->assertArrayHasKey('type', $result);
        $this->assertEquals($news->type, $result['type']);

        $this->assertArrayHasKey('date', $result);
        $this->assertEquals($news->display_date, $result['date']);

        $this->assertArrayHasKey('date_formatted', $result);
        $this->assertEquals('12/05/2017', $result['date_formatted']);

        $this->assertArrayHasKey('title', $result);
        $this->assertEquals($news->subject, $result['title']);

        $this->assertArrayHasKey('excerpt', $result);
        $this->assertEquals($news->getExcerpt(), $result['excerpt']);

        $this->assertArrayHasKey('body', $result);
        $this->assertEquals($news->body, $result['body']);

        $this->assertArrayHasKey('sources', $result);
        $this->assertIsArray($result['sources']);
        $this->assertCount(0, $result['sources']);
        
        $this->assertArrayHasKey('path', $result);
        $this->assertEquals('/news/' . $news->id, $result['path']);

        $this->assertArrayHasKey('attachments', $result);
        $this->assertEquals([], $result['attachments']);
    }

    /** @test */
	public function it_returns_type_for_news()
	{
        $news = $this->persist(DomainNews::class, [
            'type' => 'news', 
        ]);

        $newsResource = new News($news);

        $result = $newsResource->toArray();

        $this->assertArrayHasKey('type', $result);
        $this->assertEquals('news', $result['type']);
    }

    /** @test */
	public function it_returns_type_for_event()
	{
        $news = $this->persist(DomainNews::class, [
            'type' => 'event', 
        ]);

        $newsResource = new News($news);

        $result = $newsResource->toArray();

        $this->assertArrayHasKey('type', $result);
        $this->assertEquals('event', $result['type']);
    }

    /** @test */
    public function it_returns_array_with_file_attachment()
	{
        $news = $this->persist(DomainNews::class, [
            'type' => 'news', 
            'subject' => 'Foo bar', 
            'body' => 'Lorem ipsum dolor',
            'display_date' => '2017-05-12 00:00:00',
        ]);

        $attachment = $this->persist(DomainAttachment::class);

        $news->attachments()->attach($attachment);

        $newsResource = new News($news);

        $result = $newsResource->toArray();

        $this->assertArrayHasKey('attachments', $result);
        $this->assertCount(1, $result['attachments']);
        $this->assertEquals(
            '<a href="http://www.example.com/uploads/abc123.png" class="News__attachment">Scarica l\'allegato</a>', 
            $result['attachments'][0]['html']
        );
    }

    /** @test */
    public function it_returns_array_with_multiple_file_attachments()
	{
        $news = $this->persist(DomainNews::class, [
            'type' => 'news', 
            'subject' => 'Foo bar', 
            'body' => 'Lorem ipsum dolor',
            'display_date' => '2017-05-12 00:00:00',
        ]);

        $attachment = $this->persist(DomainAttachment::class);
        $anotherAttachment = $this->persist(DomainAttachment::class);

        $news->attachments()->attach($attachment);
        $news->attachments()->attach($anotherAttachment);

        $newsResource = new News($news);

        $result = $newsResource->toArray();

        $this->assertArrayHasKey('attachments', $result);
        $this->assertCount(2, $result['attachments']);
        $this->assertEquals(
            '<a href="http://www.example.com/uploads/abc123.png" class="News__attachment">Scarica l\'allegato</a>', 
            $result['attachments'][0]['html']
        );
        $this->assertEquals(
            '<a href="http://www.example.com/uploads/abc123.png" class="News__attachment">Scarica l\'allegato</a>', 
            $result['attachments'][1]['html']
        );
    }

    /** @test */
    public function it_returns_array_moving_youtube_videos_at_the_top()
	{
        $news = $this->persist(DomainNews::class, [
            'type' => 'news', 
            'subject' => 'Foo bar', 
            'body' => 'Lorem ipsum dolor',
            'display_date' => '2017-05-12 00:00:00',
        ]);

        $attachment = $this->persist(DomainAttachment::class);
        $anotherAttachment = $this->persist(DomainAttachment::class, [
            'mimetype' => '',
            'permalink' => 'https://www.youtube.com/watch?v=kJa2kwoZ2a4'
        ]);

        $news->attachments()->attach($attachment);
        $news->attachments()->attach($anotherAttachment);

        $newsResource = new News($news);

        $result = $newsResource->toArray();

        $this->assertArrayHasKey('attachments', $result);
        $this->assertCount(2, $result['attachments']);
        
        $this->assertEquals(
            '<div class="News__video"><iframe width="560" height="315" src="https://www.youtube.com/embed/kJa2kwoZ2a4" frameborder="0" allowfullscreen></iframe></div>', 
            $result['attachments'][0]['html']
        );
        $this->assertEquals('youtube', $result['attachments'][0]['type']);
        
        $this->assertEquals(
            '<a href="http://www.example.com/uploads/abc123.png" class="News__attachment">Scarica l\'allegato</a>', 
            $result['attachments'][1]['html']
        );
        $this->assertEquals('', $result['attachments'][1]['type']);
    }

    /** @test */
    public function it_returns_date_with_display_date()
	{
        $news = $this->persist(DomainNews::class, [
            'type' => 'news', 
            'subject' => 'Foo bar', 
            'body' => 'Lorem ipsum dolor',
            'published_at' => null,
            'display_date' => '2017-05-12 00:00:00',
        ]);

        $newsResource = new News($news);

        $result = $newsResource->toArray();

        $this->assertArrayHasKey('date', $result);
        $this->assertNotNull($news->display_date);
        $this->assertNull($news->published_at);
        $this->assertEquals($news->display_date, $result['date']);
    }

    /** @test */
    public function it_returns_date_with_published_at()
	{
        $news = $this->persist(DomainNews::class, [
            'type' => 'news', 
            'subject' => 'Foo bar', 
            'body' => 'Lorem ipsum dolor',
            'published_at' => '2023-05-12 00:00:00',
            'display_date' => null
        ]);

        $newsResource = new News($news);

        $result = $newsResource->toArray();

        $this->assertArrayHasKey('date', $result);
        $this->assertNotNull($news->published_at);
        $this->assertNull($news->display_date);
        $this->assertEquals($news->published_at, $result['date']);
    }

     /** @test */
	public function it_returns_sources()
	{
        $news = $this->persist(DomainNews::class, [
            'type' => 'news', 
            'subject' => 'Foo bar', 
            'body' => 'Lorem ipsum dolor',
            'display_date' => '2017-05-12 00:00:00',
        ]);

        $author = $this->persist(DomainAuthor::class, [
            'slug' => 'alessandro-piona',
            'title' => 'Dott.',
            'first_name' => 'Alessandro',
            'last_name' => 'Piona',
            'company' => 'Studio Piona'
        ]);

        $news->authors()->attach($author);

        $newsResource = new News($news);

        $result = $newsResource->toArray();

        $this->assertArrayHasKey('sources', $result);
        
        $source = $result['sources'];

        $this->assertIsArray($source);
        $this->assertCount(1, $source);

        $this->assertArrayHasKey('slug', $source[0]);
        $this->assertEquals('alessandro-piona', $source[0]['slug']);
        
        $this->assertArrayHasKey('author', $source[0]);
        $this->assertEquals('Dott. Alessandro Piona', $source[0]['author']);
        
        $this->assertArrayHasKey('company', $source[0]);
        $this->assertEquals('Studio Piona', $source[0]['company']);
    }

    /** @test */
	public function it_returns_json()
	{
        $newsResource = new News(
            $news = $this->persist(DomainNews::class, [
                'type' => 'news', 
                'subject' => 'Foo bar', 
                'body' => 'Lorem ipsum dolor',
                'display_date' => '2017-05-12 00:00:00',
            ])
        );

        $result = json_decode($newsResource->toJson(), $assoc = true);

        $this->assertArrayHasKey('id', $result);
        $this->assertEquals($news->id, $result['id']);

        $this->assertArrayHasKey('type', $result);
        $this->assertEquals($news->type, $result['type']);

        $this->assertArrayHasKey('date', $result);
        $this->assertEquals($news->display_date, $result['date']);

        $this->assertArrayHasKey('date_formatted', $result);
        $this->assertEquals('12/05/2017', $result['date_formatted']);

        $this->assertArrayHasKey('title', $result);
        $this->assertEquals($news->subject, $result['title']);

        $this->assertArrayHasKey('excerpt', $result);
        $this->assertEquals($news->getExcerpt(), $result['excerpt']);

        $this->assertArrayHasKey('body', $result);
        $this->assertEquals($news->body, $result['body']);

        $this->assertArrayHasKey('sources', $result);
        $this->assertIsArray($result['sources']);
        $this->assertCount(0, $result['sources']);
        
        $this->assertArrayHasKey('path', $result);
        $this->assertEquals('/news/' . $news->id, $result['path']);

        $this->assertArrayHasKey('attachments', $result);
        $this->assertEquals([], $result['attachments']);
    }
}