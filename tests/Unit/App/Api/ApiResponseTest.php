<?php

namespace Tests\Unit\App\Api;

use \App\Testing\AdminTestCase;
use \App\Api\ResponseFactory;
use \App\Api\NewsResponseFactory;
use \App\Api\ApiResponse;
use \App\Api\ResponseData;
use \App\Api\Resources\News as NewsResource;
use \App\Api\Contracts\Jsonable;
use \App\Api\Contracts\Arrayable;
use \App\Domain\News;
use \Carbon\Carbon;

class ApiResponseTest extends AdminTestCase
{
    /** @test */
	public function it_can_be_instantiated()
	{
        $instance = new ApiResponse(new ResponseData([]));

        $this->assertNotNull($instance);
	}

    /** @test */
	public function it_impements_arrayable_interface()
	{
        $instance = new ApiResponse(new ResponseData([]));

        $this->assertInstanceOf(Arrayable::class, $instance);
	}

    /** @test */
	public function it_impements_jsonable_interface()
	{
        $instance = new ApiResponse(new ResponseData([]));

        $this->assertInstanceOf(Jsonable::class, $instance);
	}

    /** @test */
	public function it_return_api_response()
	{
        $response = new ResponseData([]);
        $instance = new ApiResponse($response);

        $result = $instance->toArray();

        $this->assertArrayHasKey('ok', $result);
        $this->assertTrue($result['ok']);

        $this->assertArrayHasKey('data', $result);
        $this->assertEquals($response->toArray(), $result['data']);

        $this->assertArrayHasKey('message', $result);
        $this->assertEquals('', $result['message']);
	}

    /** @test */
	public function it_return_api_response_with_exception()
	{
        // Create a stub for the SomeClass class.
        $responseStub = $this->createStub(ResponseData::class);

        // Configure the stub.
        $responseStub->method('toArray')
            ->will($this->throwException(new \Exception));

        $instance = new ApiResponse($responseStub);

        $result = $instance->toArray();

        $this->assertArrayHasKey('ok', $result);
        $this->assertFalse($result['ok']);

        $this->assertArrayHasKey('data', $result);
        $this->assertEquals([], $result['data']);

        $this->assertArrayHasKey('message', $result);
        $this->assertEquals('', $result['message']);
	}
}