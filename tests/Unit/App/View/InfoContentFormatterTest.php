<?php

namespace App\View;

use \PHPUnit\Framework\TestCase;

use \App\View\Content\Title;
use \App\View\Content\Element;
use \App\View\Content\MultilineElement;
use \App\View\Content\Tag;

class InfoContentFormatterTest extends TestCase
{
	private $formatter;

	/** @before */
	protected function setUpInstance()
	{
		$this->formatter = ContentFormatterFactory::make()->createInfoFormatter(null);
	}

	/** @test */
	public function it_can_be_instantiated()
	{
		$this->assertNotNull($this->formatter);
	}

	/** @test */
	public function it_parses_services_content()
	{
		$content = "
			-Foo

			*Foobar*
			-Foo
			-Bar
			-Baz
			
			*Barbaz*
			-Baz
			-Bar
			-Foo
		";
		$markup = $this->formatter->setContent($content)->get();

		$this->assertEquals('<p>Foo</p><h3>Foobar</h3><p>Foo</p><p>Bar</p><p>Baz</p><h3>Barbaz</h3><p>Baz</p><p>Bar</p><p>Foo</p>', $markup);
	}
}