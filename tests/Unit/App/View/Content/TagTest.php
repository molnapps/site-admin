<?php

namespace App\View;

use \PHPUnit\Framework\TestCase;

use \App\View\Content\Tag;

class TagTest extends TestCase
{
	/** @test */
	public function it_can_be_instantiated()
	{
	    $this->assertNotNull(new Tag('div'));
	}

	/** @test */
	public function it_returns_markup()
	{
	    $this->assertEquals(
	    	'<div></div>', 
	    	(new Tag('div'))->build()
	    );
	}

	/** @test */
	public function it_returns_markup_with_content()
	{
	    $this->assertEquals(
	    	'<div>foobar</div>', 
	    	(new Tag('div'))->build('foobar')
	    );
	}

	/** @test */
	public function it_returns_markup_with_attributes()
	{
	    $this->assertEquals(
	    	'<div class="Foobar">foobar</div>', 
	    	(new Tag('div', ['class' => 'Foobar']))->build('foobar')
	    );
	}

	/** @test */
	public function it_returns_just_open_tag()
	{
	    $this->assertEquals(
	    	'<div>', 
	    	(new Tag('div'))->open()->build('foobar')
	    );
	}

	/** @test */
	public function it_returns_just_open_tag_with_attributes()
	{
	    $this->assertEquals(
	    	'<div ref="Foo" class="Bar">', 
	    	(new Tag('div', ['ref' => 'Foo', 'class' => 'Bar']))->open()->build('foobar')
	    );
	}

	/** @test */
	public function it_returns_just_close_tag()
	{
	    $this->assertEquals(
	    	'</div>', 
	    	(new Tag('div'))->close()->build('foobar')
	    );
	}

	/** @test */
	public function it_returns_just_close_tag_with_attributes()
	{
	    $this->assertEquals(
	    	'</div>', 
	    	(new Tag('div', ['ref' => 'Foo', 'class' => 'Bar']))->close()->build('foobar')
	    );
	}

	/** @test */
	public function it_returns_single_tag()
	{
	    $this->assertEquals(
	    	'<img>', 
	    	(new Tag('img'))->single()->build('foobar')
	    );
	}

	/** @test */
	public function it_returns_single_tag_with_attributes()
	{
	    $this->assertEquals(
	    	'<img src="_img/image.jpg">', 
	    	(new Tag('img', ['src' => '_img/image.jpg']))->single()->build('foobar')
	    );
	}
}