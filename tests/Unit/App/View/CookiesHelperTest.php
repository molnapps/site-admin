<?php

namespace App\View;

use App\View\CookiesHelper;
use \PHPUnit\Framework\TestCase;

class CookiesHelperTest extends TestCase
{
	/** @test */
	public function it_will_block_cookies_for_youtube_video()
	{
		$result = CookiesHelper::iframe(
			'<iframe width="560" height="315" src="https://www.youtube.com/embed/483IAsGUcbg" frameborder="0" allowfullscreen></iframe>'
		);

		$this->assertEquals('<iframe class="_iub_cs_activate" width="560" height="315" suppressedsrc="https://www.youtube.com/embed/483IAsGUcbg" src="//cdn.iubenda.com/cookie_solution/empty.html" frameborder="0" allowfullscreen></iframe>', $result);

		$this->assertStringContainsString('<iframe', $result);
		$this->assertStringContainsString('allowfullscreen', $result);
		$this->assertStringContainsString('frameborder="0"', $result);
		$this->assertStringContainsString('</iframe>', $result);

		$this->assertStringContainsString('width="560"', $result);
		$this->assertStringContainsString('height="315', $result);

		$this->assertStringContainsString(
			'class="_iub_cs_activate"', 
			$result
		);

		$this->assertStringContainsString(
			'suppressedsrc="https://www.youtube.com/embed/483IAsGUcbg"', 
			$result
		);

		$this->assertStringContainsString(
			'src="//cdn.iubenda.com/cookie_solution/empty.html"', 
			$result
		);
	}

	/** @test */
	public function it_will_block_cookies_for_google_maps()
	{
		$result = CookiesHelper::iframe(
			'<iframe 
				src="https://www.google.com/maps/embed/v1/place?q=Via+Pietro+Mascagni%2C+24+20122%2C+Milano&key=abc123"
				frameborder="0" 
				style="border:0" 
				allowfullscreen
			></iframe>'
		);

		$this->assertStringContainsString('<iframe', $result);
		$this->assertStringContainsString('allowfullscreen', $result);
		$this->assertStringContainsString('frameborder="0"', $result);
		$this->assertStringContainsString('style="border:0"', $result);
		$this->assertStringContainsString('</iframe>', $result);

		$this->assertStringContainsString(
			'class="_iub_cs_activate"', 
			$result
		);

		$this->assertStringContainsString(
			'suppressedsrc="https://www.google.com/maps/embed/v1/place?q=Via+Pietro+Mascagni%2C+24+20122%2C+Milano&key=abc123"', 
			$result
		);

		$this->assertStringContainsString(
			'src="//cdn.iubenda.com/cookie_solution/empty.html"', 
			$result
		);
	}

	/** @test */
	public function it_will_block_cookies_for_script()
	{
		$result = CookiesHelper::script(
			'<script type="text/javascript" src="https://unpkg.com/molnapps-partners@^1.0.6/dist/partners.js"></script>'
		);

		$this->assertStringContainsString('<script', $result);
		$this->assertStringContainsString('type="text/plain"', $result);
		$this->assertStringContainsString('data-suppressedsrc="https://unpkg.com/molnapps-partners@^1.0.6/dist/partners.js"', $result);
		$this->assertStringContainsString('class="_iub_cs_activate"', $result);
		$this->assertStringContainsString('</script>', $result);
	}

	/** @test */
	public function it_will_block_cookies_for_script_without_type()
	{
		$result = CookiesHelper::script(
			'<script src="foobar.js"></script>'
		);

		$this->assertStringContainsString('<script', $result);
		$this->assertStringContainsString('type="text/plain"', $result);
		$this->assertStringContainsString('data-suppressedsrc="foobar.js"', $result);
		$this->assertStringContainsString('class="_iub_cs_activate"', $result);
		$this->assertStringContainsString('</script>', $result);
	}

	/** @test */
	public function it_will_returns_script_tags_from_array()
	{
		$result = CookiesHelper::script()->fromUrls(['foobar.js']);

		$this->assertStringContainsString('<script', $result);
		$this->assertStringContainsString('type="text/plain"', $result);
		$this->assertStringContainsString('data-suppressedsrc="foobar.js"', $result);
		$this->assertStringContainsString('class="_iub_cs_activate"', $result);
		$this->assertStringContainsString('</script>', $result);
	}
}