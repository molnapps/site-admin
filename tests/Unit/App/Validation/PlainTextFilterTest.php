<?php

namespace App\View;

use \PHPUnit\Framework\TestCase;

use \App\Validation\PlainTextFilter;

class PlainTextFilterTest extends TestCase
{
	private $filter;

	/** @before */
	protected function setUpInstance()
	{
		$this->filter = new PlainTextFilter;
	}

	/** @test */
	public function it_can_be_instantiated()
	{
		$this->assertNotNull($this->filter);
	}

	/** @test */
	public function it_strips_tags()
	{
	    $this->assertEquals('Hello world', $this->filter->filter('<b>Hello <i>world</i></b>'));
	}

	/** @test */
	public function it_will_not_convert_quotes()
	{
	    $this->assertEquals("L'allerta", $this->filter->filter("L'allerta"));
	}

	/** @test */
	public function it_will_not_be_100_percent_safe_against_xss()
	{
	    $this->assertEquals(
	    	'" onmouseover="javascript:doSomethingEvil()"', 
	    	$this->filter->filter('" onmouseover="javascript:doSomethingEvil()"')
	    );
	}
}