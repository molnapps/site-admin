<?php

namespace App\FrontEnd;

use \PHPUnit\Framework\TestCase;

use org\bovigo\vfs\vfsStream;

class ViewTest extends TestCase
{
	protected $root;
    
    protected function setUp(): void
    {
        $this->root = vfsStream::setup('views', null, [
        	'home.php' => 'Homepage',
        	'about.php' => 'About',
        	'info.php' => 'Info',
        ]);
    }

	/** @test */
	public function it_can_be_instantiated()
	{
		$view = new View;
		$this->assertNotNull($view);
	}

	/** @test */
	public function it_accepts_a_base_path()
	{
		$view = new View;
		$this->assertEquals('/about.php', $view->getPath('about'));
		$view->setBasePath($this->root->url());
		$this->assertEquals('vfs://views/about.php', $view->getPath('about'));
	}

	/** @test */
	public function it_invokes_the_default_view_if_no_page_is_set_in_request()
	{
		$view = new View;
		$view->setBasePath($this->root->url());
		$output = $view->invokeAndReturn();
		
		$this->assertEquals('Info', $output);
	}

	/** @test */
	public function it_gets_a_view_from_the_request()
	{
		$_GET['page'] = 'about';

		$view = new View;
		$view->setBasePath($this->root->url());
		$output = $view->invokeAndReturn();
		
		$this->assertEquals('About', $output);
	}

	/** @test */
	public function it_allows_to_set_a_custom_default_view()
	{
		$view = new View;
		$view->setBasePath($this->root->url());
		$view->setDefaultPage('about');
		$output = $view->invokeAndReturn();
		
		$this->assertEquals('About', $output);
	}

	/** @test */
	public function it_finds_a_title_for_a_view_or_returns_default_title()
	{
		$view = new View;
		$view->setTitleDictionary([
			'about' => 'About us',
			'info' => 'Information',
		]);
		
		$this->assertEquals('About us', $view->getTitle('about'));
		$this->assertEquals('Untitled', $view->getTitle('foobar'));
	}
}