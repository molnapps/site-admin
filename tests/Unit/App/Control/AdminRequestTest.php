<?php

namespace Tests\Unit\App\Control;

use \PHPUnit\Framework\TestCase;

use \App\Control\AdminRequest;

use \App\Domain\Collection;
use \App\Domain\News;

class AdminRequestTest extends TestCase
{
	/** @test */
	public function it_accepts_a_domain_object()
	{
		$request = new AdminRequest;

		$news = new News;
		
		$request->addObject($news);

		$this->assertEquals($news, $request->getObject('news'));
	}

	/** @test */
	public function it_accepts_a_collection()
	{
		$request = new AdminRequest;

		$newsCollection = new Collection([], new News, new News);

		$request->addObject($newsCollection);

		$this->assertEquals($newsCollection, $request->getObject('newsCollection'));
	}
}