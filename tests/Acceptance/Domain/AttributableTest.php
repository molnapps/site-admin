<?php

namespace App\Domain;

use \PHPUnit\Framework\TestCase;

class AttributableTest extends TestCase
{
	/** @test */
	public function it_accepts_a_property()
	{
		$page = new Page;
		$page->setProperty('foo', 'bar');
		$this->assertEquals('bar', $page->getProperty('foo'));
	}

	/** @test */
	public function it_accepts_many_properties()
	{
		$page = new Page;
		$page->setProperties([
			'foo' => 'bar',
			'bar' => 'baz',
		]);

		$this->assertEquals('bar', $page->getProperty('foo'));
		$this->assertEquals('baz', $page->getProperty('bar'));
		$this->assertEquals(['foo' => 'bar', 'bar' => 'baz'], $page->getProperties());
	}

	/** @test */
	public function it_asserts_if_a_property_is_dirty()
	{
		$page = new Page;

		$page->initializeProperties([
			'foo' => 'foo',
		]);

		$this->assertFalse($page->isDirty('foo'));

		$page->setProperty('foo', 'bar');

		$this->assertTrue($page->isDirty('foo'));
	}

	/** @test */
	public function it_resets_properties_before_initializing()
	{
		$page = new Page;

		$page->setProperties([
			'foo' => 'bar',
			'bar' => 'baz',
		]);

		$this->assertEquals('bar', $page->getProperty('foo'));
		$this->assertEquals('baz', $page->getProperty('bar'));

		$page->initializeProperties([
			'foo' => 'foo',
		]);

		$this->assertEquals('foo', $page->getProperty('foo'));
		$this->assertNull($page->getProperty('bar'));
	}

	/** @test */
	public function it_asserts_that_a_property_exists()
	{
		$page = new Page;

		$this->assertFalse($page->propertyExists('foo'));

		$page->setProperty('foo', 'bar');

		$this->assertTrue($page->propertyExists('foo'));
	}

	/** @test */
	public function it_asserts_that_has_a_property()
	{
		$page = new Page;

		$this->assertFalse($page->hasProperty('foo'));

		$page->setProperty('foo', 'bar');

		$this->assertTrue($page->hasProperty('foo'));

		$page->setProperty('foo', false);

		$this->assertFalse($page->hasProperty('foo'));
	}

	/** @test */
	public function it_returns_fillable_properties()
	{
		$page = new Page;

		$this->assertEquals([
			'slug' => null, 
			'title' => null, 
			'heroImage' => null, 
			'body' => null, 
			'published_at' => null,
			'language' => null,
		], $page->getFillableProperties());

		$assignments = [
			'slug' => 'alessandro-piona',
			'title' => 'Alessandro Piona',
			'heroImage' => 'alessandro-piona.png',
			'body' => 'Lorem ipsum dolor sit amet',
			'published_at' => '2017-02-18 00:00:00',
			'language' => 'it_IT',
		];

		$page->setProperties($assignments);
		$page->setProperty('foo', 'bar');

		$this->assertEquals($assignments, $page->getFillableProperties());
	}
}