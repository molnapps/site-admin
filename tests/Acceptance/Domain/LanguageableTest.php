<?php

namespace App\Domain;

use \App\Testing\AdminTestCase;

class LanguageableTest extends AdminTestCase
{
	/** @test */
	public function it_accepts_a_language()
	{
		$page = new Page;
		$page->language = 'it_IT';
		$this->assertEquals('it_IT', $page->language);
	}

	/** @test */
	public function it_does_not_accept_invalid_language()
	{
		$page = new Page;
		$page->language = 'foobar';
		$this->assertNull($page->language);
	}

	/** @test */
	public function it_applies_a_custom_scope()
	{
		// Assuming I have an italian version AND an english version of the same slug
		$this->persist(Page::class, ['language' => 'it_IT', 'slug' => 'alessandro-piona', 'title' => 'Alessandro Piona', 'body' => 'Biografia']);
		$this->persist(Page::class, ['language' => 'en_US', 'slug' => 'alessandro-piona', 'title' => 'Alessandro Piona', 'body' => 'Biography']);
		
		$collection = Page::available()->find();
		
		$this->assertCount(1, $collection);
		$this->assertEquals('it_IT', $collection->getFirst()->language);
	}
}