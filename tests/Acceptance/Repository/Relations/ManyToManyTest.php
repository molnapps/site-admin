<?php

namespace Tests\Acceptance\App\Repository\Relations;

use \App\Repository\Relations\ManyToMany;
use \App\Database\TableGatewayFactory;

use \App\Domain\News;
use \App\Domain\Attachment;
use \App\Domain\Collection;

use \App\Testing\AdminTestCase;

class ManyToManyTest extends AdminTestCase
{
	private $pivotTable;

	/** @before */
	protected function setUpTableGateway()
	{
		$this->pivotTable = TableGatewayFactory::register('attachments_news');
		$this->relativesTable = TableGatewayFactory::register('attachments');
	}
	
	/** @after */
	protected function tearDownTableGateway()
	{
		TableGatewayFactory::reset();
	}

	/** @test */
	public function it_attaches_some_relatives_to_a_parent()
	{
		$relation = $this->createManyToManyRelationship(new News(['id' => 1]));
		$relation->attach(new Attachment(['id' => 1]));
		$relation->attach(new Attachment(['id' => 2]));
		
		$this->assertEquals([
			['news_id' => '1', 'attachment_id' => '1'],
			['news_id' => '1', 'attachment_id' => '2'],
		], $this->pivotTable->select());
	}

	/** @test */
	public function it_detaches_one_relative_from_a_parent()
	{
		$this->linkNewsToAttachment(1, 1);
		$this->linkNewsToAttachment(1, 2);
		$this->linkNewsToAttachment(2, 1);
		
		$this->assertEquals([
			['news_id' => 1, 'attachment_id' => 1],
			['news_id' => 1, 'attachment_id' => 2],
			['news_id' => 2, 'attachment_id' => 1],
		], $this->pivotTable->select());

		$relation = $this->createManyToManyRelationship(new News(['id' => 1]));

		// Detach with null
		$relation->detach(null);
		$this->assertEquals([
			['news_id' => 1, 'attachment_id' => 1],
			['news_id' => 1, 'attachment_id' => 2],
			['news_id' => 2, 'attachment_id' => 1],
		], $this->pivotTable->select());

		// Detach first attachment
		$relation->detach(new Attachment(['id' => 2]));
		$this->assertEquals([
			['news_id' => 1, 'attachment_id' => 1],
			['news_id' => 2, 'attachment_id' => 1],
		], $this->pivotTable->select());

		// Detach second attachment
		$relation->detach(new Attachment(['id' => 1]));
		$this->assertEquals([
			['news_id' => 2, 'attachment_id' => 1],
		], $this->pivotTable->select());
	}

	/** @test */
	public function it_detaches_all_relatives_from_a_parent()
	{
		$this->linkNewsToAttachment(1, 1);
		$this->linkNewsToAttachment(1, 2);
		$this->linkNewsToAttachment(2, 1);
		
		$this->assertEquals([
			['news_id' => 1, 'attachment_id' => 1],
			['news_id' => 1, 'attachment_id' => 2],
			['news_id' => 2, 'attachment_id' => 1],
		], $this->pivotTable->select());

		// Detach all attachments
		$relation = $this->createManyToManyRelationship(new News(['id' => 1]));
		$relation->reset();
		
		$this->assertEquals([
			['news_id' => 2, 'attachment_id' => 1],
		], $this->pivotTable->select());
	}

	/** @test */
	public function it_wont_detach_all_relatives_from_all_parents()
	{
		$this->linkNewsToAttachment(1, 1);
		$this->linkNewsToAttachment(1, 2);
		$this->linkNewsToAttachment(2, 1);

		$this->assertEquals([
			['news_id' => 1, 'attachment_id' => 1],
			['news_id' => 1, 'attachment_id' => 2],
			['news_id' => 2, 'attachment_id' => 1],
		], $this->pivotTable->select());

		// Try to detach all attachments without a parent id
		$this->expectException(\Exception::class);
		$this->expectExceptionMessage('You cannot reset all parent ids');
		$relation = $this->createManyToManyRelationship(new News);
		$relation->reset();
		
		$this->assertEquals([
			['news_id' => 1, 'attachment_id' => 1],
			['news_id' => 1, 'attachment_id' => 2],
			['news_id' => 2, 'attachment_id' => 1],
		], $this->pivotTable->select());
	}

	/** @test */
	public function it_gets_all_relatives_for_a_parent()
	{
		$attachment1 = Attachment::create(['title' => '', 'permalink' => '']);
		$attachment2 = Attachment::create(['title' => '', 'permalink' => '']);

		$this->linkNewsToAttachment(1, 1);
		$this->linkNewsToAttachment(1, 2);
		$this->linkNewsToAttachment(2, 1);

		$relation1 = $this->createManyToManyRelationship(new News(['id' => 1]));
		$this->assertInstanceOf(Collection::class, $relation1->get());
		$this->assertCount(2, $relation1->get());

		$relation2 = $this->createManyToManyRelationship(new News(['id' => 2]));
		$this->assertInstanceOf(Collection::class, $relation2->get());
		$this->assertCount(1, $relation2->get());
	}

	/** @test */
	public function it_returns_empty_collection_if_parent_has_no_relative()
	{
		$attachment1 = Attachment::create(['title' => '', 'permalink' => '']);
		
		$this->linkNewsToAttachment(2, 1);

		$relation = $this->createManyToManyRelationship(new News(['id' => 1]));
		$this->assertInstanceOf(Collection::class, $relation->get());
		$this->assertCount(0, $relation->get());
	}

	/** @test */
	public function it_returns_empty_collection_if_no_parent_has_any_relative()
	{
		$relation1 = $this->createManyToManyRelationship(new News);
		$this->assertCount(0, $relation1->get());
	}

	/** @test */
	public function it_gets_all_relatives_for_all_parents()
	{
		$attachment1 = Attachment::create(['title' => '', 'permalink' => '']);
		$attachment2 = Attachment::create(['title' => '', 'permalink' => '']);
		
		$this->linkNewsToAttachment(1, 1);
		$this->linkNewsToAttachment(1, 2);
		$this->linkNewsToAttachment(2, 1);

		$relation = $this->createManyToManyRelationship(new News);

		$this->assertCount(2, $relation->get()[1]);
		$this->assertCount(1, $relation->get()[2]);
	}

	private function createManyToManyRelationship(News $parent)
	{
		$relative = new Attachment;
		$relative->setTableGateway($this->relativesTable);
		return new ManyToMany($this->pivotTable, $parent, $relative, $relative);
	}

	private function linkNewsToAttachment($newsId, $attachmentId)
	{
		$this->pivotTable->insert(['news_id' => $newsId, 'attachment_id' => $attachmentId]);
	}
}