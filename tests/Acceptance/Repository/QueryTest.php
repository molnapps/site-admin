<?php

namespace Tests\Acceptance\App\Repository\Query;

use \PHPUnit\Framework\TestCase;

use \App\Repository\QueryBuilder;
use \App\Repository\Query;

class QueryTest extends TestCase
{
	/** @test */
	public function it_builds_a_query_instance()
	{
		$query = QueryBuilder::create()->getQuery();

		$this->assertInstanceOf(Query::class, $query);
	}

	/** @test */
	public function it_builds_a_query_with_simple_where_clause()
	{
		$query = QueryBuilder::create()->where(['id' => 1])->getQuery();

		$this->assertEquals([
			'where' => [
				['id', 'eq', 1],
			],
		], $query->toArray());
	}

	/** @test */
	public function it_builds_a_query_with_two_simple_where_clauses()
	{
		$query = QueryBuilder::create()->where(['id' => 1, 'body' => 'Lorem ipsum'])->getQuery();

		$this->assertEquals([
			'where' => [
				['id', 'eq', 1], 
				['body', 'eq', 'Lorem ipsum'],
			]
		], $query->toArray());
	}

	/** @test */
	public function it_builds_a_query_with_complex_where_clause()
	{
		$query = QueryBuilder::create()->where([
			['id', 'gt', 1],
			['body', 'contains', 'Lorem ipsum']
		])->getQuery();

		$this->assertEquals([
			'where' => [
				['id', 'gt', 1], 
				['body', 'contains', 'Lorem ipsum'],
			]
		], $query->toArray());
	}

	/** @test */
	public function it_builds_a_query_without_overriding_multiple_where_clauses()
	{
		$query = QueryBuilder::create()
			->where(['accountId' => 1])
			->where(['id' => 1])
			->getQuery();

		$this->assertEquals([
			'where' => [
				['accountId', 'eq', 1], 
				['id', 'eq', 1],
			]
		], $query->toArray());
	}

	/** @test */
	public function it_builds_a_query_with_mixed_where_clause()
	{
		$query = QueryBuilder::create()->where([
			'id' => 1,
			['body', 'contains', 'Lorem ipsum']
		])->getQuery();

		$this->assertEquals([
			'where' => [
				['id', 'eq', 1], 
				['body', 'contains', 'Lorem ipsum'],
			]
		], $query->toArray());
	}

	/** @test */
	public function it_builds_a_query_with_a_columns_list()
	{
		$query = QueryBuilder::create()->columns(['id', 'body'])->getQuery();

		$this->assertEquals([
			'columns' => ['id', 'body']
		], $query->toArray());
	}

	/** @test */
	public function it_builds_a_composite_query()
	{
		$query = QueryBuilder::create()->columns(['id', 'body'])->where(['id' => 1])->getQuery();

		$this->assertEquals([
			'columns' => ['id', 'body'],
			'where' => [
				['id', 'eq', 1]
			]
		], $query->toArray());
	}

	/** @test */
	public function it_builds_a_composite_query_with_a_limit()
	{
		$query = QueryBuilder::create()
			->columns(['id', 'body'])
			->where(['id' => 1])
			->limit(10)
			->getQuery();

		$this->assertEquals([
			'columns' => ['id', 'body'],
			'where' => [
				['id', 'eq', 1]
			],
			'limit' => 10,
		], $query->toArray());
	}

	/** @test */
	public function it_builds_a_composite_query_with_a_limit_and_offset()
	{
		$query = QueryBuilder::create()
			->columns(['id', 'body'])
			->where(['id' => 1])
			->limit(10)
			->offset(5)
			->getQuery();

		$this->assertEquals([
			'columns' => ['id', 'body'],
			'where' => [
				['id', 'eq', 1]
			],
			'limit' => 10,
			'offset' => 5,
		], $query->toArray());
	}

	/** @test */
	public function it_builds_a_composite_query_with_a_limit_and_first_page()
	{
		$query = QueryBuilder::create()
			->columns(['id', 'body'])
			->where(['id' => 1])
			->limit(10)
			->page(1)
			->getQuery();

		$this->assertEquals([
			'columns' => ['id', 'body'],
			'where' => [
				['id', 'eq', 1]
			],
			'limit' => 10,
		], $query->toArray());
	}

	/** @test */
	public function it_builds_a_composite_query_with_a_limit_and_n_page()
	{
		$query = QueryBuilder::create()
			->columns(['id', 'body'])
			->where(['id' => 1])
			->limit(10)
			->page(2)
			->getQuery();

		$this->assertEquals([
			'columns' => ['id', 'body'],
			'where' => [
				['id', 'eq', 1]
			],
			'limit' => 10,
			'offset' => 10,
		], $query->toArray());
	}

	/** @test */
	public function it_builds_a_query_with_specified_order()
	{
		$query = QueryBuilder::create()
			->columns(['id', 'body', 'subject'])
			->orderBy(['id'])
			->getQuery();

		$this->assertEquals([
			'columns' => ['id', 'body', 'subject'],
			'order' => [0 => 'id'],
		], $query->toArray());
	}
}