<?php

namespace Tests\Admin;

use \App\Testing\AdminTestCase;
use \App\Base\AppContainer;

class SignInTest extends AdminTestCase
{
    /** @after */
    protected function tearDownFailedAttemptsManager()
    {
        AppContainer::get('signin.failedAttemptsManager')->reset();
    }

    /** @test */
    public function it_displays_signin_form_if_not_authenticated()
    {
        $this
            ->visit('')
            ->seeSignInPage();
    }

    /** @test */
    public function it_signs_a_user_in_with_valid_credentials()
    {
        $this
            ->visit('')
            ->fillSignInForm('admin', 'secret')
            ->submit()
            ->seeDashboard();
    }

    /** @test */
    public function it_wont_sign_a_user_in_with_invalid_credentials()
    {
        $this
            ->visit('')
            ->fillSignInForm('foo', 'bar')
            ->submit()
            ->seeSignInPage();
    }

    /** @test */
    public function it_wont_sign_a_user_in_without_credentials()
    {
        $this
            ->visit('')
            ->fillSignInForm('', '')
            ->submit()
            ->seeSignInPage();
    }

    /** @test */
    public function it_will_not_sign_a_user_in_after_5_failed_attempts()
    {
        for ($i = 0; $i < 5; $i++) {
            $this->visit('')->fillSignInForm('foo', 'bar')->submit();
            $this->seeSignInPage();
        } 
        $this->visit('')->fillSignInForm('admin', 'secret')->submit();
        $this->seeSignInPage();
    }

    /** @test */
    public function it_will_sign_a_user_in_after_3_failed_attempts()
    {
        for ($i = 0; $i < 3; $i++) {
            $this->visit('')->fillSignInForm('foo', 'bar')->submit();
            $this->seeSignInPage();
        } 
        $this->visit('')->fillSignInForm('admin', 'secret')->submit();
        $this->seeDashboard();
    }

	/** @test */
	public function it_displays_dashboard_if_authenticated()
	{
		$this
            ->signedIn()
            ->visit('')
            ->seeDashboard();
	}

    /** @test */
    public function it_wont_authenticate_without_a_token()
    {
        $this
            ->visit('')
            ->fillSignInForm('admin', 'secret')
            ->submitWithoutToken()
            ->seeInvalidRequestPage();
    }

    private function fillSignInForm($username, $password)
    {
        return $this
            ->seeForm('signInForm')
                ->seeInput('username')
                    ->enter($username)
                ->seeInput('password')
                    ->enter($password);
    }
}