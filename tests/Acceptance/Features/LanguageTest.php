<?php

namespace Tests\Admin\News;

use \App\Base\AppContainer;
use \App\Testing\AdminTestCase;

class LanguageTest extends AdminTestCase
{
    private $languageProvider;

    protected function shouldBootstrapLanguageProvider()
    {
        return false;
    }

    /** @before */
    protected function setUpLanguageProvider()
    {
        $this->languageProvider = AppContainer::get('languageProvider');
    }

    /** @test */
    public function it_asks_for_a_language_if_none_is_set()
    {
        $this->languageProvider->setAvailableLanguages([
            'it_IT' => 'Italiano',
            'en_US' => 'English',
        ]);

        $this->languageProvider->setCurrentLanguage(null);
        
        $this
            ->signedIn()
            ->visit('')
            ->seeTitle('Lingua')
            ->click('Italiano')
            ->seeDashboard();

        $this->assertEquals('it_IT', $this->languageProvider->getCurrentLanguage());
    }

    /** @test */
    public function it_sets_the_language_if_only_one_language_is_supported()
    {
        $this->languageProvider->setAvailableLanguages([
            'it_IT' => 'Italiano',
        ]);

        $this->languageProvider->setCurrentLanguage(null);

        $this
            ->signedIn()
            ->visit('')
            ->seeDashboard();

        $this->assertEquals('it_IT', $this->languageProvider->getCurrentLanguage());
    }

    /** @test */
    public function it_does_not_ask_for_language_if_a_language_is_set()
    {
        $this->languageProvider->setAvailableLanguages([
            'it_IT' => 'Italiano',
            'en_US' => 'English',
        ]);

        $this->languageProvider->setCurrentLanguage('it_IT');

        $this
            ->signedIn()
            ->visit('')
            ->seeDashboard();

        $this->assertEquals('it_IT', $this->languageProvider->getCurrentLanguage());
    }

    /** @test */
    public function it_displays_languages_if_language_could_not_be_set()
    {
        $this->languageProvider->setAvailableLanguages([
            'it_IT' => 'Italiano',
            'en_US' => 'English',
        ]);

        $this->languageProvider->setCurrentLanguage(null);

        $this
            ->signedIn()
            ->visit('SetLanguage', ['language' => 'foobar'])
            ->seeTitle('Lingua');

        $this->assertNull($this->languageProvider->getCurrentLanguage());
    }

    /** @test */
    public function it_displays_a_list_of_available_languages_if_no_language_is_provided()
    {
        $this->languageProvider->setAvailableLanguages([
            'it_IT' => 'Italiano',
            'en_US' => 'English',
        ]);

        $this->languageProvider->setCurrentLanguage('it_IT');

        $this
            ->signedIn()
            ->visit('SetLanguage')
            ->seeTitle('Lingua');
    }
}