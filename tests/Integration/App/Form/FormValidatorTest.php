<?php

namespace App\Form;

use \PHPUnit\Framework\TestCase;

use App\Control\AdminRequest;
use App\Domain\Attachment;

class FormValidatorTest extends TestCase
{
	private $request;
	private $instance;

	/** @before */
	protected function setUpInstance()
	{
		$this->request = new AdminRequest;
		$domainObject = new Attachment();
		$this->instance = new FormValidator($this->request, $domainObject);
	}

	/** @test */
	public function it_can_be_instantiated()
	{
		$this->assertNotNull($this->instance);
	}

	/** @test */
	public function it_returns_false_if_required_field_is_missing()
	{
		$this->assertFalse($this->instance->validate(['title' => 'required']));
	}

	/** @test */
	public function it_returns_true_if_required_field_was_submitted()
	{
		$this->request->title = 'Foobar';
		
		$this->assertTrue($this->instance->validate(['title' => 'required']));
	}

	/** @test */
	public function it_returns_false_if_uploaded_file_is_required_and_no_file_was_submitted()
	{
		$this->assertFalse($this->instance->validate(['attachment' => 'required:uploadedFile']));
	}

	/** @test */
	public function it_returns_true_if_uploaded_file_is_required_and_a_file_was_submitted()
	{
		$this->request->files->add([
			'attachment' => [
				'name' => 'test.png',
				'type' => 'image/png',
				'tmp_name' => __DIR__ . '/../../../data/test.png',
				'error' => UPLOAD_ERR_OK,
				'size' => '1024',
			]
		]);
		
		$this->assertTrue($this->instance->validate(['attachment' => 'required:uploadedFile']));
	}

	/** @test */
	public function it_appends_validation_errors_to_request()
	{
		$this->instance->validate(['title' => 'required', 'attachment' => 'required:uploadedFile']);

		$this->assertTrue($this->request->hasValidationErrors());
		$this->assertCount(1, $this->request->getValidationMessages('attachment.title'));
		$this->assertCount(1, $this->request->getValidationMessages('attachment.attachment'));
	}

	/** @test */
	public function it_allows_to_pass_a_custom_rule_and_returns_false_if_validation_fails()
	{
		$validatorStub = $this->createMock(Validator::class);
		$validatorStub->method('validate')->willReturn(false);
		$this->assertFalse($this->instance->validate([$validatorStub]));
	}

	/** @test */
	public function it_allows_to_pass_a_custom_rule_and_returns_true_if_validation_succeeds()
	{
		$validatorStub = $this->createMock(Validator::class);
		$validatorStub->method('validate')->willReturn(true);
		$this->assertTrue($this->instance->validate([$validatorStub]));
	}

	/** @test */
	public function it_appends_validation_errors_to_request_with_custom_validator()
	{
		$validatorStub = $this->createMock(Validator::class);
		$validatorStub->method('validate')->willReturn(false);
		$validatorStub->expects($this->once())->method('addValidationError');

		$this->instance->validate([$validatorStub]);
	}
}